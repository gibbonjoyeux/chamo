
# CHAMO

**chamo** is a framework made to create images with sound or sound with images :)

With **chamo**, you code sound like you would draw in graphic frameworks like **Processing**.
You shape sound with code and visualize or save it (as .wav files).

## EXAMPLES

**chamo** provides some example modules (in the `doc/` directory) to show you how you could use it.

## BUILD

**chamo** uses the **lua** library. Once installed, you only need to run `build.sh` which will do the rest :)

## RUN

Two modes are available in **chamo**, the **interactive mode** (by default) with
image, sound and user events and the **Non real time mode** made to compute
sound file as fast as possible.

Both require the module path

- `> chamo my_scipt.lua [PARAM 1] [PARAM 2]`
- `> chamo -nrt my_scipt.lua [PARAM 1] [PARAM 2]`

Optional parameters can be passed to your script after the script path.
They will be available as `init()` parameters.
Be aware that each of your arguments will be passed as string, so if you need
numbers, you will need to use the `tonumber()` lua function.

# PROGRAM STRUCTURE

```
function	init(param1, param2, ...)
	-- 48000hz 24bps
	specs(48000, 24)
	format("centered", 100)
end

function	update()
	-- DRAW CIRCLE (with A4 frequency)
	circle(0, 0, 50, 48000 / freq("A4"))
	-- SAVE FILE AFTER 5sec
	if time() > 5 then
		save("audio.wav")
	end
end
```

**chamo** requires 2 principal functions, `init()` and `update()`.

`init()` is made to set basic variables such as **specs**, **dimensions** and your own. It is called first.

`update()` is then called repeatedly.

## SAMPLERATE & DIMENSIONS

`specs(freq, bps)` is used to set the output samplerate and bit per second, 48000 samples per second and 32 bits per sample are the default values.

`format(mode, [width])` is used to define the space in which you'd like to work in.
By default, the space is set to **sound** which uses the space range of 1:-1 (from top to down, left to right).
It is made to emulate standard sound space (x -> [-1,1] y -> [1,-1]).
You can also use **classic** and **centered** mode.

Both **classic** and **centered** require a *width* as 2nd parameter.
The **classic** mode is intended to draw. It uses the regular space of graphical environment (x & y -> [0,width])
The **centered** mode is more intended to make sounds as it puts 0 in the middle of the screen (x & y -> [-width,width])

## WHAT IS A SAMPLE ?

The samplerate defines how many points your sound have each second.
If your samplerate is defined to 44100 for example, there will be 44100 points in a second.
