
local function		init( data,
					freq, amp,
					atk, sus, rel,
					freq_freq, freq_amp,
					amp_freq, amp_amp )
	--- INIT TIME
	data.t = 0
	data.p = 0
	--- INIT FREQ & AMP
	data.freq = freq or 440
	data.amp = amp or 0.5
	--- INIT ENV
	data.atk = atk or 0.1
	data.sus = data.atk + ( sus or 0.1 )
	data.rel = data.sus + ( rel or 0.1 )
	--- INIT MODULATION FREQ
	data.freq_freq = freq_freq or 0
	data.freq_amp = freq_amp or 0
	--- INIT MODULATION AMP
	data.amp_freq = amp_freq or 0
	data.amp_amp = amp_amp or 0
end

local function		update( data )
	local			freq
	local			amp

	--- COMPUTE TIME
	data.t = data.t + 1 / 48000
	--- COMPUTE FREQ
	freq = data.freq + sin( data.t * data.freq_freq ) * data.freq_amp
	data.p = data.p + freq / 48000
	--- COMPUTE ENV
	if data.t < data.atk then
		amp = map( data.t, 0, data.atk, 0, 1 )
		--amp = amp * amp
	elseif data.t < data.sus then
		amp = 1
	elseif data.t < data.rel then
		amp = map( data.t, data.sus, data.rel, 1, 0 )
		--amp = amp * amp
	else
		return nil
	end
	--- COMPUTE AMP
	amp = amp
	--[[--]] * data.amp
	--[[--]] * wave.sin( data.t * data.amp_freq, 1, 1 - data.amp_amp )
	--- COMPUTE SOUND
	return cos( data.p ) * amp, sin( data.p ) * amp
end

return init, update
