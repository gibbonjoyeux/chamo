
local			super = {
	layer = true,			-- Sound layering mode
	block = 32,				-- Sound block size
	block_x = {},
	block_y = {},
	path = '/',				-- Loading path
	voices = {}				-- Voices array
}

--------------------------------------------------------------------------------
--- INIT
--------------------------------------------------------------------------------

function		init()
	samplerate( 48000 )
	format( "centered", 100 )

	for i = 1, super.block do
		super.block_x[ i ] = 0
		super.block_y[ i ] = 0
	end
end

--------------------------------------------------------------------------------
--- UPDATE
--------------------------------------------------------------------------------

function		update()
	local		x, y

	--- COMPUTE SOUND
	for _, voice in ipairs( super.voices ) do
		--- VOICE ACTIVE
		if voice.state == true then
			--- COMPUTE VOICE BLOCK
			for i = 1, super.block do
				x, y = voice_update( voice )
				super.block_x[ i ] = super.block_x[ i ] + x
				super.block_y[ i ] = super.block_y[ i ] + y
				if voice.state == false then
					break
				end
			end
		end
	end
	--- SEND SOUND
	for i = 1, super.block do
		point( super.block_x[ i ] * 100, super.block_y[ i ] * 100 )
		super.block_x[ i ] = 0
		super.block_y[ i ] = 0
	end
end

function		voice_new( func_init, func_update, ... )
	local		data

	--- INIT DATA
	data = {}
	func_init( data, ... )
	--- ADD VOICE
	---- FIND AVAILABLE VOICE
	for i, voice in pairs( super.voices ) do
		if voice.state == false then
			voice.state = true
			voice.data = data
			voice.update = func_update
			return
		end
	end
	---- CREATE NEW VOICE
	super.voices[ #super.voices + 1 ] = {
		state = true,
		data = data,
		update = func_update
	}
end

function		voice_update( voice )
	local		x, y

	--- UPDATE VOICE
	x, y = voice.update( voice.data )
	--- REMOVE VOICE
	if x == nil then
		voice.state = false
		return 0, 0
	end
	--- SEND SOUND
	return x, y
end

--------------------------------------------------------------------------------
--- OSC RULES
--------------------------------------------------------------------------------

osc = {}
osc.synth = {}
osc.filter = {}
osc.mode = {}

--------------------------------------------------
--- SYNTHS
--------------------------------------------------

function		osc.synth.load( path, name )
	local		state
	local		func_init, func_update

	--- CHECK
	if type( path ) ~= 'string' or type( name ) ~= 'string' then
		return
	end
	--- LOAD MODULE
	state, func_init, func_update = pcall( dofile, super.path .. path .. '.lua' )
	--- ADD MODULE
	if func_init and func_update then
		print( '[SYNTH] Loaded ' .. name  )
		osc.synth[ name ] = function( ... )
			voice_new( func_init, func_update, ... )
		end
	end
end

--------------------------------------------------
--- FILTERS
--------------------------------------------------

function		osc.filter.load( path, name )
	local		state
	local		func_init, func_update

	if type( path ) ~= 'string' or type( name ) ~= 'string' then
		return
	end
	state, func_init, func_update = pcall( dofile, super.path .. path .. '.lua' )
	func_init( {} )
	if func_init and func_update then
		print( '[FILTER] Loaded ' .. name  )
		osc.synth[ name ] = function( ... )
			voice_new( func_init, func_update, ... )
		end
	end
end

function		osc.filter.set( name, ... )
end

--------------------------------------------------
--- MODE
--------------------------------------------------

function		osc.mode.path( path )
	print( '[MODE] Path: ' .. path )
	super.path = path .. '/'
end

function		osc.mode.layer( layer )
	print( '[MODE] Sound layer: ' .. layer )
	if mode == 0 then
		super.layer = false
	else
		super.layer = true
	end
end

function		osc.mode.block( len )
	print( '[MODE] Block length: ' .. len )
	super.block = len
	for i = 1, super.block do
		super.block_x[ i ] = 0
		super.block_y[ i ] = 0
	end
end
