# URGENT BEFORE TRACKER

## NEW API

- [ ] KEYBOARD
	- [ ] `function chamo.keydown()`
	- [ ] `function chamo.keyrelease()`
- [ ] MOUSE
	- [ ] `function chamo.mousemove()`
	- [ ] `function chamo.mousedown()`
	- [ ] `function chamo.mouserelease()`
- [ ] OSC
	- [ ] `function chamo.osc()`		Called on OSC message

## API

- [x] ease & wave merged. Using an output range of 0-1
- [x] Allow wave functions to map with extra parameters (ex: wave.squ(x, -1, 1))
- [x] Other sound waves:
	- [x] hsine (see sunvox, half-sine or half wave rectified)
	- [x] asine (see sunvox, abs-sine or full wave rectified)
- [x] `specs(framerate, bitspersample)` (empty chunks & reset indexes)
- [x] `noise(octaves, x, [y, [z]])` for open simplex noise
- [ ] `samplerate()`
- [x] `lerp()`
- [ ] Events
	- [ ] `mouse.x` & `mouse.y`
	- [ ] `mouse.down`
	- [ ] `mouse.click`
	- [ ] `key.down`
	- [ ] `key.value`
	- [ ] `midi.xxx`
- [ ] Being able to use drawing functions (line, circle, etc) by passing `t`, a point in time, to be able to draw
      these shapes point by point (draw after draw) and not the entire shape in one time.
- [x] stack / pool system
- [x] FORMAT only used in save / print step but not before
	- [x] remap samples at save / draw / output
	- [x] remove `get_param_coord_{x,y}()`
- [x] Use vectors in matrix and other space stuff
- [x] Use `ptrdiff_t` (int type) as integer when getting lua parameters
- [ ] Draw curves
	- [x] `curve(x0, y0, x1, y1, x2, y2, x3, y3, points)`	-> cubic bezier curve
	- [x] `curve(x0, y0, x1, y1, x2, y2, x3, y3, points)`	-> quadratic bezier curve
- [ ] Draw shapes
	- [x] `shape.begin()`
	- [x] `shape.record()`
	- [x] `shape.complete(points)`
	- [x] `shape.vertex(x, y)` -> linear vertex
	- [x] `shape.vertex(x0, y0, x, y)` -> quadratic vertex
	- [x] `shape.vertex(x0, y0, x1, y1, x, y)` -> cubic vertex
	- [ ] `shape.vertex(rx, ry, a, fb, fd, x, y)` -> arc vertex
	- [x] `shape.draw(my_shape, points)`
	- [ ] `shape.draw(my_shape, points, from, portion)` to draw slice of the shape (from & to being percentages)
	- [x] `shape.get(my_shape, t)` to get shape point coord at t [0-1]
	- [ ] `shape.load(svg_path)`
	- [x] remove `fpoly()`
- [x] Remove `record_start()` & `record_end()`
- [x] Remove FPS mode
	- [x] Remove time constants
	- [x] Remove `mode()`
	- [x] Remove `samples_max()`
	- [x] `samples_count()` to `samplecount()`
- [x] Clean API in README.md
- [x] Check Matrix system (translation does not use previous set angle, which is weird...)

## CODE

- [x] Use `lst_new_ex()` when possible for speed / memory optimization
- [x] Use only DOUBLE values when saving samples. Translation is done after
- [x] Recycle matrix chained list when poped
- [x] Remove float from drawing c functions (replace with double)
- [ ] Some compilation functions still use exit functions such as `exit_fail()`



# TRACKER STEPS

## THINK ABOUT

- [ ] Pattern subdivision (like abbleton). A pattern can have cells of 4/8/16/32/64/ lines (or anything else), allowing the user to have some patterns really precise and other not or even no regular (like 3 or 6 or whatever)
- [ ] Pattern subdivision would imply compiled data structure to use linked lists as cells will have the ability to have non-regular size

## STATIC DATA STRUCTURE

- [x] Split SDL init and SDL audio device creation (Only one SDL init,
and audio device creation each live run)
- [x] Easily open/close LUA and SDL audio device
- [x] Check if multiple .lua scripts can be loaded
- [ ] Define tracker data structure
- [ ] Create main functions to interact with the track structure
	- [ ] add / del			variable
	- [ ] add / del			event function (easing)
	- [ ] add / del			source
	- [ ] add / del			module (del -> remove module from others input)
	- [ ] set				module input
	- [ ] add / del / move	pattern
	- [ ] add / del			pattern lines
	- [ ] set				pattern cell
	- [ ] add / del			pattern event

## DYNAMIC DATA STRUCTURE & COMPILATION

- [ ] Define computed tracker data structure
	- [ ] Think about how time passes (bpm, lines, samples, etc.)
	- [ ] Think about "default" variables
		- [ ] BPM (variating)
		- [ ] Volume variables
	- [ ] C structures and all
- [ ] Create dynamic track
	- [ ] Merge timeline patterns into a track
	- [ ] Handle track modules calls usable (disable, enable, samples, etc)
- [ ] Function to handle track cells value
	- [ ] Create value from a string (note, num, str, var, ope)
	- [ ] Push value to lua stack
- [ ] Build tracker base
	- [ ] Update variables
		- [ ] Progress through lines with BPM variable
		- [ ] With default easing functions
		- [ ] With user defined event functions
	- [ ] Dispatch variables
		- [ ] Send variables value to lua operations state
		- [ ] Send variables value to lua default state (as globals)
	- [ ] Call modules
		- [ ] Call modules `init()`
		- [ ] Call modules `update()`
			- [ ] Param - Input (default)
			- [ ] Param - Current sample (default)
			- [ ] Param - Number of samples (default)
			- [ ] Param - Parameters (user)
- [ ] Save track as .wav
- [ ] Compute what's before time 0 but not save it
- [ ] Play track live (from specific line)
- [ ] Save track to .ct file (chamo track file extension)
- [ ] Load .ct files

## USER INTERFACE

- [ ] Create basic UI structure / functions
	- [ ] Draw text
	- [ ] Define UI / buttons data structure (inspired from raylib UI data structure)
	- [ ] Basic button
	- [ ] Basic check box
	- [ ] Basic slider
	- [ ] AE slider (zone slider)
	- [ ] Text input
	- [ ] Zones to draw (wave zone, track zone, events zone... Redraw only changed zones)

## EXTRA

- [ ] Allow binary modules (with the use of named pipes (FIFO))
	- [ ] Run bin in parallel, able to kill the bin process
	- [ ] Send init() params as argv
	- [ ] Chamo send update() params to FIFO
	- [ ] Bin reads it and sends points to FIFO
	- [ ] Chamo read bin points

## OTHER

- [ ] Compilation should only use sample index. Line index is (in data structure) is useless most of the time
- [ ] Easing function formated like `func(from, to, progress)`. Allow user to more easily build usable event modules (like LFO or anything).




# FEATURES

- [x] In interactive mode, reload .lua script when updated
- [x] 2 display modes, Vector & Time (x,y & time,y)
- [ ] Time display mode print both left and right at the same time (2 curves)
- [ ] Can write math operation in cells (ex: `var_1 / 2 + var_lfo`)
- [ ] Choose module output (stdout or another module)
- [ ] Tracker handling binary modules via named pipes (FIFO). Argv is used as
		init() and the fifo file is used as update(). Chamo send parameters
		(and info) and the bin send samples (to the same file).

# OPTIMIZATION

- [ ] Use LuaJIT library to optimize lua
