################################################################################
## SOURCES #####################################################################
################################################################################

SRCS			= 	$(wildcard src/*.c) \
					$(wildcard src/*/*.c) \
					$(wildcard src/*/*/*.c) \
					$(wildcard src/*/*/*/*.c)
OBJS			= 	$(SRCS:src/%.c=obj/%.o)
INCS			=	$(wildcard inc/*.h)

NAME			= chamo
CC				= gcc

INCLUDES		=	-I inc/ \
					-I lib/c-basics/inc/ \
					-I lib/c-string/inc/ \
					-I lib/c-linked-list/inc/ \
					-I lib/c-vec/ \
					-I lib/luajit/src/

FLAGS_PROJECT	= 	lib/c-basics/c-basics.a \
					lib/c-string/c-string.a \
					lib/c-linked-list/c-linked-list.a \
					-I lib/luajit/src/ \
					-L lib/luajit/src/ \
					-lluajit \
					-lm \
					-framework SDL2

FLAGS_FILE		=	-Wall -Wextra -Werror -g

################################################################################
## RULES #######################################################################
################################################################################

##############################
## RULE BINARY ###############
##############################
all: $(NAME)

run: all
	@./$(NAME)

$(NAME): libraries $(OBJS)
	@#$(CC) $(FLAGS_PROJECT) -o $(NAME) $(OBJS)
	@$(CC) $(OBJS) -o $(NAME) $(FLAGS_PROJECT)

##############################
### RULE SOURCES #############
##############################
obj/%.o: src/%.c Makefile $(INCS)
	@mkdir -p $(dir $(@))
	@echo $(<:src/%=%)
	@$(CC) $(FLAGS_FILE) $(INCLUDES) -o $(@) -c $<

libraries:
	@echo "[lib/c-basics]" \
	&& [[ -f lib/c-basics/c-basics.a ]] \
	|| make -s -C lib/c-basics/
	@echo "[lib/c-string]" \
	&& [[ -f lib/c-string/c-string.a ]] \
	|| make -s -C lib/c-string/
	@echo "[lib/c-linked-list]" \
	&& [[ -f lib/c-linked-list/c-linked-list.a ]] \
	|| make -s -C lib/c-linked-list/
	@echo "[lib/luajit]" \
	&& [[ -f lib/luajit/src/libluajit.a ]] \
	|| make -s -C lib/luajit/
	@echo "[chamo]"

##############################
### RULE CLEANING ############
##############################
clean:
	@rm -rf obj/

fclean: clean
	@rm -rf $(NAME)

##############################
### RULE RESTART #############
##############################
re: fclean
	@make
