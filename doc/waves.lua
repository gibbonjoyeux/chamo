
--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(44100)
	format("centered", 100)
end

function		update()

	if time() > 10 then
		save("audio.wav")
	end

	local	t
	local	i
	local	x, y
	local	amp
	local	duty

	rotate(time() * 0.5)
	amp = 40
	i = time() * 440
	t = floor((time() / 2) % 5)
	-- SIN WAVE
	if t == 0 then
		x = cos(i)
		y = sin(i)
	-- SQUARE WAVE
	elseif t == 1 then
		x = wave_squ(i)
		y = wave_squ(i + 0.25)
	-- SAW WAVE
	elseif t == 2 then
		x = wave_saw(i)
		y = wave_saw(i + 0.5)
	-- TRIANGLE WAVE
	elseif t == 3 then
		x = wave_tri(i)
		y = wave_tri(i + 0.25)
	-- PULSE WAVE
	elseif t == 4 then
		duty = ((time() / 2) % 1) * 100
		x = wave_pul(i, duty)
		y = wave_pul(i + 0.25, duty)
	end
	point(x * amp, y * amp)
end
