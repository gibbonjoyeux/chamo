--[ GIBBON JOYEUX ]--

function		init()
	samplerate(44100)
	format("centered", 100)

	i = 0
end

function		update()
	local		notes = {523.25, 659.25, 783.99}

	if time() >= 10 then
		save("audio.wav")
	end

	rotate(time() * notes[1])
	point(50, 0, 1)
end
