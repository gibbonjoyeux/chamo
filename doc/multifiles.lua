
--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(44100)
	format("centered", 100)
end

function		update()
	for i = 1, 3 do
		pushmatrix()
			for j = 1, 360 do
				rotate(1 / 360)
				point(30 * i, 0)
			end
		popmatrix()
		save("circle-" .. i .. ".wav", true)
	end
	exit()
end
