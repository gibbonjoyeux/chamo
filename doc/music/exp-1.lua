
--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(44100)
	format("centered", 100)
end

function		update()

	if time() > 10 then
		save("audio.wav")
	end

	local	t
	local	x, y
	local	amp

	rotate(time() * 2)
	t = time() * 440
	amp = 40
	x = map(sin(t / 4), -1, 1, -100, 100)
	y = sin(t) * amp
	point(x, y)
end
