
function		put_point(x, y, amp)
	point(x * amp, y * amp)
end

function		create_wave(path, fn1, off1, fn2, off2, extra)
	local		t

	-- STATIC
	while time() < len do
		t = time() * freq
		put_point(fn1(t + off1, extra), fn2(t + off2, extra), amp)
	end
	save(path .. ".wav", true)
	-- ROTATE LEFT
	while time() < len do
		t = time() * freq
		pushmatrix()
			rotate(time() / len)
			put_point(fn1(t + off1, extra), fn2(t + off2, extra), amp)
		popmatrix()
	end
	save(path .. "-rotate-l.wav", true)
	-- ROTATE RIGHT
	while time() < len do
		t = time() * freq
		pushmatrix()
			rotate(-time() / len)
			put_point(fn1(t + off1, extra), fn2(t + off2, extra), amp)
		popmatrix()
	end
	save(path .. "-rotate-r.wav", true)
end

--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(48000)
	format("centered", 100)
	freq = 440
	amp = 50
	len = 2
	scale_len = 0.5
end

function		update()
	-- SIN WAVE
	create_wave("wave-sin", cos, 0, sin, 0)
	-- TRIANGLE WAVE
	create_wave("wave-tri", wavetri, 0, wavetri, 0.25)
	-- SQUARE WAVE
	create_wave("wave-squ", wavesqu, 0, wavesqu, 0.25)
	create_wave("wave-squ-mono", wavesqu, 0, wavesqu, 0)
	-- SAWTOOTH WAVE
	create_wave("wave-saw", wavesaw, 0, wavesaw, 0.5)
	create_wave("wave-saw-mono", wavesaw, 0, wavesaw, 0)

	exit()
end
