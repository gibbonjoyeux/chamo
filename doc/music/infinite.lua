
--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(44100)
	format("centered", 100)
end

function		update()

	if time() > 10 then
		save("infinite.wav")
	end

	local	t
	local	x, y
	local	amp

	t = time() * 440 / 2
	amp = 50
	x = map(sin(t / 2), -1, 1, -100, 100)
	y = sin(t) * amp
	point(x, y)
end
