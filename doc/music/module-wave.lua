
function		get_wave_point(wave, t, pulse)
	-- SIN WAVE
	if wave == "sin" then
		return cos(t), sin(t)
	elseif wave == "sin-mono" then
		return cos(t), cos(t)
	-- TRIANGLE WAVE
	elseif wave == "tri" then
		return wave_tri(t), wave_tri(t + 0.25)
	elseif wave == "tri-mono" then
		return wave_tri(t), wave_tri(t)
	-- SQUARE WAVE
	elseif wave == "squ" then
		return wave_squ(t), wave_squ(t + 0.25)
	elseif wave == "squ-mono" then
		return wave_squ(t), wave_squ(t)
	-- SAWTOOTH WAVE
	elseif wave == "saw" then
		return wave_saw(t), wave_saw(t + 0.5)
	elseif wave == "saw-mono" then
		return wave_saw(t), wave_saw(t)
	-- PULSE WAVE
	elseif wave == "pulse" then
		return wave_pul(t, pulse), wave_pul(t + 0.5, pulse)
	elseif wave == "pulse-mono" then
		return wave_pul(t, pulse), wave_pul(t, pulse)
	end
	return 0, 0
end

