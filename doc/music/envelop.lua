
--------------------------------------------------------------------------------
-- PARAMETERS:
-- . WAVE TYPE		(sin | sin-mono | tri | tri-mono | squ | squ-mono
--					| saw | saw-mono | pulse | pulse-mono)
-- . FREQ IN		(frequency at start, ex: 196)
-- . FREQ OUT		(frequency at end, ex: 65.41)
-- . AMPLITUDE		(0-100)
-- . ATTACK EASE	(linear | quad-in | quad-out | quad-inout | ...)
-- . RELEASE EASE
-- . ATTACK TIME
-- . SUSTAIN TIME
-- . RELEASE TIME
-- . [ROTATION]		(number of turn)
-- . [NOISE]		(0-100)
-- . [PULSE DUTY]	(for pulse | puslse-mono wave types, 0-100)
-- . [NAME]	 		output_name | "_specs"
--------------------------------------------------------------------------------

require "module-wave"
require "module-ease"

function		init(wave_type, frequency_in, frequency_out, amplitude,
				attack_ease, release_ease, attack_time, sustain_time,
				release_time, rot, noise_amp, pulse_duty, name_mode)
	-- SPECS
	samplerate(48000)
	format("centered", 100)
	-- PARAMETERS
	wave = wave_type
	freq_in = tonumber(frequency_in)
	freq_out = tonumber(frequency_out)
	attack = tonumber(attack_time)
	sustain = tonumber(sustain_time)
	release = tonumber(release_time)
	ease_attack = get_ease_func(attack_ease)
	ease_release = get_ease_func(release_ease)
	amp = tonumber(amplitude)
	noise = tonumber(noise_amp)
	pulse = tonumber(pulse_duty)
	total = attack + sustain + release
	rotation = rot or 0
	-- OUTPUT NAME
	if name_mode == "_specs" then
		name = "envelop-" .. wave
		.. "-" .. ((freq_in == freq_out) and freq_in or (freq_in .. "_" .. freq_out))
		.. "-" .. attack .. "_" .. sustain .. "_" .. release
		.. "-" .. rotation ..".wav"
	elseif name_mode ~= nil then
		name = name_mode .. ".wav"
	else
		name = "envelop.wav"
	end
end

function		update()
	local		x, y
	local		t, wt
	local		freq

	point_noise(noise)
	-- DRAW SOUND
	while time() < total do
		t = time()
		freq = map(t, 0, total, freq_in, freq_out)
		wt = time() * freq
		rotate(rotation * (1 / (48000 * total)))
		matrix_push()
			-- ATTACK
			if t < attack then
				scale(ease_attack(map(t, 0, attack, 0, 1)))
			-- RELEASE
			elseif t > attack + sustain then
				scale(1 - ease_release(map(t - attack - sustain, 0, release, 0, 1)))
			end
			-- DRAW POINT
			x, y = get_wave_point(wave, wt, pulse)
			point(x * amp, y * amp)
		matrix_pop()
	end
	-- SAVE FILE
	save(name)
end
