
--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(44100)
	format("centered", 100)

	freq1 = 440 / 2
	freq2 = 523.25 / 2
	freq3 = 659.25 / 2
	amp = 100
end

function		update()
	local	x, y
	local	t1, t2, t3
	local	x1, y1, x2, y2, x3, y3
	local	amp1, amp2, amp3

	amp1 = 1
	amp2 = 0.7
	amp3 = 0.3
	t1 = time() * freq1
	t2 = time() * freq2
	t3 = time() * freq3

	x1 = cos(t1)
	y1 = sin(t1)
	x2 = wave_squ(t2)
	y2 = wave_squ(t2 + 0.25)
	x3 = wave_tri(t3)
	y3 = wave_tri(t3 + 0.25)

	x = (x1 * amp1 + x2 * amp2 + x3 * amp3) / (amp1 + amp2 + amp3)
	y = (y1 * amp1 + y2 * amp2 + y3 * amp3) / (amp1 + amp2 + amp3)
	point(x * amp, y * amp)

	if time() > 10 then
		save("audio.wav")
	end

end
