

function		get_ease_func(name)
	if name == "sine-in" then return ease_sine_in
	elseif name == "sine-out" then return ease_sine_out
	elseif name == "sine-inout" then return ease_sine_inout
	elseif name == "quad-in" then return ease_quad_in
	elseif name == "quad-out" then return ease_quad_out
	elseif name == "quad-inout" then return ease_quad_inout
	elseif name == "cubic-in" then return ease_sine_in
	elseif name == "cubic-out" then return ease_sine_out
	elseif name == "cubic-inout" then return ease_sine_inout
	elseif name == "quart-in" then return ease_sine_in
	elseif name == "quart-out" then return ease_sine_out
	elseif name == "quart-inout" then return ease_sine_inout
	elseif name == "quint-in" then return ease_sine_in
	elseif name == "quint-out" then return ease_sine_out
	elseif name == "quint-inout" then return ease_sine_inout
	elseif name == "exp-in" then return ease_sine_in
	elseif name == "exp-out" then return ease_sine_out
	elseif name == "exp-inout" then return ease_sine_inout
	elseif name == "circ-in" then return ease_sine_in
	elseif name == "circ-out" then return ease_sine_out
	elseif name == "circ-inout" then return ease_sine_inout
	elseif name == "back-in" then return ease_sine_in
	elseif name == "back-out" then return ease_sine_out
	elseif name == "back-inout" then return ease_sine_inout
	elseif name == "elastic-in" then return ease_sine_in
	elseif name == "elastic-out" then return ease_sine_out
	elseif name == "elastic-inout" then return ease_sine_inout
	elseif name == "bounce-in" then return ease_sine_in
	elseif name == "bounce-out" then return ease_sine_out
	elseif name == "bounce-inout" then return ease_sine_inout
	else return ease_linear end
end

