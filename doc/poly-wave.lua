--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	bpm = 100
	freq = 220
end

function		update()
	local		t
	local		total_points
	local		vertices

	vertices = map(sin(time_bpm(bpm)), -1, 1, 3, 50)
	points = 48000 / freq

	polygon(0, 0, 50, vertices, points, time() / 2)

	if time() >= 10 then
		save("audio.wav")
	end
end
