
function		draw_b()
	line(0, 0, 0, 20, points)
	line(0, 20, 10, 15, points)
	line(10, 15, 0, 10, points)
	line(0, 10, 10, 5, points)
	line(10, 5, 0, 0, points)
end

function		draw_i()
	line(0, 0, 0, 20, points)
end

function		draw_s()
	line(10, 0, 0, 7, points)
	line(0, 7, 10, 14, points)
	line(10, 14, 0, 20, points)
end

function		draw_o()
	line(5, 0, 10, 10, points)
	line(10, 10, 5, 20, points)
	line(5, 20, 0, 10, points)
	line(0, 10, 5, 0, points)
end

function		draw_u()
	line(0, 0, 5, 20, points)
	line(5, 20, 10, 0, points)
end

function		draw_e()
	line(10, 0, 0, 0, points)
	line(0, 0, 0, 10, points)
	line(0, 10, 5, 10, points / 2, 2)
	line(0, 10, 0, 20, points)
	line(0, 20, 10, 20, points)
end

function		draw_t()
	line(5, 20, 5, 0, points)
	line(5, 0, 0, 0, points)
	line(5, 0, 10, 0, points)
end

function		init()
	samplerate(44100)
	format("centered", 100)

	points = 20
	i = 0
	prev_samples = 0
end

function		update()
	if time() > 12 then
		save("audio.wav")
	end

	if i / 200 < 2 then
		rotate(i / 200)
	end

	i = i + 1
	limit(i * 2)
	point_noise(0.5)

	push()
		-- B
		translate(-40, -28); draw_b()
		-- I
		translate(17, 0); draw_i()
		-- S
		translate(8, 0); draw_s()
		-- O
		translate(15, 0); draw_o()
		-- U
		translate(15, 0); draw_u()
		-- S
		translate(15, 0); draw_s()
	pop()

	push()
		translate(-30, 17)
		-- B
		translate(0, 0); draw_b()
		-- I
		translate(17, 0); draw_i()
		-- S
		translate(8, 0); draw_s()
		-- E
		translate(15, 0); draw_e()
		-- T
		translate(15, 0); draw_t()
	pop()

	-- CIRCLE
	point_noise(1.5)
	for j = 0, 4 do
		circle(0, 0, 50, 10, 5)
		circle(0, 0, 55, 10, 5)
	end

	-- TRACING LINE
	if time() <= 6 then
		limit()
		rotate()
		if time() > 4 then rotate(i / 200) end
		last_x, last_y = last_point()
		line(last_x, last_y, 0, 0, map(time(), 3, 6, 100, 0))
	end

end
