
--------------------------------------------------------------------------------
-- MODULES
--------------------------------------------------------------------------------

--------------------------------------------------
-- DELAY
--------------------------------------------------

m_delay = {}

-- CREATE DELAY OBJECT
m_delay.new =	function(seconds)
	local		obj
	local		len

	len = floor(seconds * 48000)
	-- CREATE REVERB OBJECT
	obj = {
		samples = {},
		i = 1,
		len = len,
		get = m_delay.get,
		compute = m_delay.compute
	}
	-- FILL SAMPLES
	for i = 1, len do
		obj.samples[i] = {0, 0}
	end
	-- RETURN
	return obj
end

-- GET POINT AT DELAY TIME
m_delay.get =		function(self)
	local			point

	point = self.samples[self.i]
	return point[1], point[2]
end

-- SEND POINT TO DELAY & RETURNS CURRENT DELAYED POINT
m_delay.compute =	function(self, x, y)
	local			sample	
	local			first_x, first_y

	-- GET 1ST SAMPLE
	sample = self.samples[self.i]
	first_x = sample[1]
	first_y = sample[2]
	-- SET IT AS LAST
	sample[1] = x
	sample[2] = y
	-- GO 1 SAMPLE FURTHER
	if self.i == self.len then
		self.i = 1
	else
		self.i = self.i + 1
	end
	return first_x, first_y
end

--------------------------------------------------
-- FILTERS
--------------------------------------------------

m_filter = {}

m_filter.new =		function(filter_type, delay, gain)
	return {
		gain = gain,
		delay = m_delay.new(delay),
		compute = m_filter[filter_type]
	}
end

m_filter.comb =		function(self, x, y)
	-- y[n] = x[n] + g·y[n−M]
	local			delay_x, delay_y

	delay_x, delay_y = self.delay:get()
	x, y = self.delay:compute(x + delay_x * self.gain, y + delay_y * self.gain)
	return x, y
end

m_filter.lowpass =	function(self, x, y)
	local			delay_x, delay_y
	local			out_x, out_y

	x = x * (1 - self.gain)
	y = y * (1 - self.gain)
	delay_x, delay_y = self.delay:get()
	out_x = x + delay_x
	out_y = y + delay_y
	self.delay:compute(out_x, out_y)
	return out_x, out_y
end

m_filter.allpass =	function(self, x, y)
	-- y[n] = (−g·x[n]) + x[n−M] + (g·y[n−M])
	local			delay_x, delay_y
	local			out_x, out_y

	delay_x, delay_y = self.delay:get()
	delay_x, delay_y = delay_x - x * self.gain, delay_y - y * self.gain
	out_x, out_y = self.delay:compute(x + delay_x * self.gain, y + delay_y * self.gain)
	return delay_x, delay_y
end

--------------------------------------------------------------------------------
-- REVERB
--------------------------------------------------------------------------------

m_reverb = {}

m_reverb.new =		function(time)
	return {
		comb_1 = m_filter.new("comb", (36.04 / 1000) * time, 0.805),
		comb_2 = m_filter.new("comb", (31.12 / 1000) * time, 0.827),
		comb_3 = m_filter.new("comb", (40.44 / 1000) * time, 0.783),
		comb_4 = m_filter.new("comb", (44.92 / 1000) * time, 0.764),

		allpass_1 = m_filter.new("allpass", (5 / 1000) * time, 0.7),
		allpass_2 = m_filter.new("allpass", (1.68 / 1000) * time, 0.7),
		allpass_3 = m_filter.new("allpass", (0.48 / 1000) * time, 0.7),

		compute = m_reverb.compute
	}
end

m_reverb.compute =	function(self, x, y)
	local			comb_x, comb_y
	local			all_x, all_y
	local			tmp_x, tmp_y

	-- COMPUTE PARALLEL COMB FILTERS
	comb_x, comb_y = self.comb_1:compute(x, y)
	tmp_x, tmp_y = self.comb_2:compute(x, y)
	comb_x, comb_y = comb_x + tmp_x, comb_y + tmp_y
	tmp_x, tmp_y = self.comb_3:compute(x, y)
	comb_x, comb_y = comb_x + tmp_x, comb_y + tmp_y
	tmp_x, tmp_y = self.comb_4:compute(x, y)
	comb_x, comb_y = comb_x + tmp_x, comb_y + tmp_y
	-- COMPUTE SERIE ALLPASS FILTERS
	all_x, all_y = self.allpass_1:compute(comb_x * 0.25, comb_y * 0.25)
	all_x, all_y = self.allpass_2:compute(all_x, all_y)
	all_x, all_y = self.allpass_3:compute(all_x, all_y)
	return all_x, all_y
end

--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(48000)
	format("centered", 100)

	amp_max = 70
	p = 0

	reverb = m_reverb.new(2)
end

function		update()
	local		notes = {freq("C4"), freq("d4"), freq("F4"), freq("G4")}
	local		note
	local		x, y
	local		amp
	local		t
	local		rev_point

	if time() > 12 then
		save("reverb.wav")
	end

	note = notes[floor(1 + (time() * 3) % 4)]
	t = (time() * 3) % 1
	if t < 0.5 then
		amp = map(t, 0, 0.5, 0, amp_max, true)
	else
		amp = map(t, 0.5, 1, amp_max, 0, true)
	end
	p = p + note / 48000
	x = wave.tri(p, -amp, amp)
	y = wave.tri(p + 0.25, -amp, amp)

	x, y = reverb:compute(x, y)
	point(x, y)
end
