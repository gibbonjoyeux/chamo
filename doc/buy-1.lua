
function		draw_b()
	line(0, 0, 0, 20, points)
	line(0, 20, 10, 15, points)
	line(10, 15, 0, 10, points)
	line(0, 10, 10, 5, points)
	line(10, 5, 0, 0, points)
end

function		draw_m()
	line(0, 20, 0, 0, points)
	line(0, 0, 10, 20, points)
	line(10, 20, 20, 0, points)
	line(20, 0, 20, 20, points)
end

function		draw_y()
	line(10, 0, 0, 20, points * 2)
	line(5, 10, 0, 0, points)
end

function		draw_s()
	line(10, 0, 0, 7, points)
	line(0, 7, 10, 14, points)
	line(10, 14, 0, 20, points)
end

function		draw_c()
	line(10, 0, 0, 0, points)
	line(0, 0, 0, 20, points)
	line(0, 20, 10, 20, points)
end

function		draw_o()
	line(5, 0, 10, 10, points)
	line(10, 10, 5, 20, points)
	line(5, 20, 0, 10, points)
	line(0, 10, 5, 0, points)
end

function		draw_n()
	line(0, 20, 0, 0, points)
	line(0, 0, 10, 20, points)
	line(10, 20, 10, 0, points)
end

function		draw_u()
	line(0, 0, 0, 20, points)
	line(0, 20, 10, 20, points)
	line(10, 20, 10, 0, points)
end

function		draw_e()
	line(10, 0, 0, 0, points)
	line(0, 0, 0, 10, points)
	line(0, 10, 5, 10, points / 2, 2)
	line(0, 10, 0, 20, points)
	line(0, 20, 10, 20, points)
end

function		draw_buy(x, y, s, a)
	matrix_push()
		translate(x, y)
		if s then scale(s) end
		if a then rotate(a) end
		-- B
		draw_b()
		-- U
		translate(15, 0); draw_u()
		-- Y
		translate(15, 0); draw_y()
	popmatrix()
end

function		draw_consume(x, y, s, a)
	matrix_push()
		translate(x, y)
		if s then scale(s) end
		if a then rotate(a) end
		-- C
		draw_c()
		-- O
		translate(15, 0); draw_o()
		-- N
		translate(15, 0); draw_n()
		-- S
		translate(15, 0); draw_s()
		-- U
		translate(15, 0); draw_u()
		-- M
		translate(15, 0); draw_m()
		-- E
		translate(20, 0); draw_e()
	matrix_pop()
end

function		init()
	samplerate(44100)
	format("centered", 100)

	rects = {}
	rect_num = 8
	points = 20
	--i = 0
	--prev_samples = 0
end

function		update()
	local		new_rects

	if time() > 10 then
		save("audio.wav")
	end

	-- FRAME
	for turn = 0, 2 do
		line(-56, -99, 56, -99, 50)
		line(56, -99, 56, 99, 50)
		line(56, 99, -56, 99, 50)
		line(-56, 99, -56, -99, 50)
	end

	-- ADD RECT
	if #rects < rect_num then
		table.insert(rects, 0.1)
	end
	-- UPDATE RECTS
	new_rects = {}
	for _, rect in pairs(rects) do
		matrix_push()
			point_noise(rect * 3)
			scale(rect)
			rotate(rand(-0.01, 0.01))
			line(-56, -99, 56, -99, 30)
			line(56, -99, 56, 99, 30)
			line(56, 99, -56, 99, 30)
			line(-56, 99, -56, -99, 30)
		matrix_pop()
		rect = rect * 1.15
		if rect < 1 then
			table.insert(new_rects, rect)
		end
	end
	rects = new_rects

	point_noise(2)
	draw_consume(-50, -5, 0.5)
end
