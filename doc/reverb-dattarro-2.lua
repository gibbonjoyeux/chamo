
--------------------------------------------------------------------------------
-- MODULES
--------------------------------------------------------------------------------

--------------------------------------------------
-- DELAY
--------------------------------------------------

m_delay = {}

-- CREATE DELAY OBJECT
m_delay.new =	function(num_samples)
	local		obj
	local		len

	-- CREATE REVERB OBJECT
	obj = {
		samples = {},
		i = 1,
		len = num_samples,
		get = m_delay.get,
		compute = m_delay.compute
	}
	-- FILL SAMPLES
	for i = 1, num_samples do
		obj.samples[i] = 0
	end
	-- RETURN
	return obj
end

-- GET POINT AT DELAY TIME
m_delay.get =		function(self, i)
	return self.samples[1 + (self.i - 1 + (i or 0)) % self.len]
end

-- SEND POINT TO DELAY & RETURNS CURRENT DELAYED POINT
m_delay.compute =	function(self, x)
	local			sample	
	local			first

	-- GET 1ST SAMPLE
	sample = self.samples[self.i]
	first = sample
	-- SET IT AS LAST
	self.samples[self.i] = x
	-- GO 1 SAMPLE FURTHER
	if self.i == self.len then
		self.i = 1
	else
		self.i = self.i + 1
	end
	return first
end

--------------------------------------------------
-- FILTERS
--------------------------------------------------

m_filter = {}

m_filter.new =		function(filter_type, delay, gain)
	return {
		gain = gain,
		delay = m_delay.new(delay),
		compute = m_filter[filter_type]
	}
end

m_filter.comb =		function(self, x)
	-- y[n] = x[n] + g·y[n−M]
	local			delay_x

	delay_x = self.delay:get()
	x = self.delay:compute(x + delay_x * self.gain)
	return x
end

m_filter.lowpass =	function(self, x)
	local			delay_x
	local			out_x

	x = x * self.gain
	delay_x = self.delay:get()
	out_x = x - delay_x * self.gain
	self.delay:compute(out_x)
	return out_x
end

m_filter.allpass =	function(self, x)
	-- y[n] = (−g·x[n]) + x[n−M] + (g·y[n−M])
	local			delay_x
	local			out_x

	delay_x = self.delay:get()
	delay_x = delay_x - x * self.gain
	out_x = self.delay:compute(x + delay_x * self.gain)
	return delay_x
end

--------------------------------------------------------------------------------
-- REVERB
--------------------------------------------------------------------------------

m_reverb = {}

m_reverb.new =		function(bandwidth, input_diff_1, input_diff_2,
					decay_diff_1, decay_diff_2, decay, damping, size)
	return {
		-- VARIABLES
		decay = decay,

		-- FILTERS
		lowpass_1 = m_filter.new("lowpass", 1, bandwidth),

		allpass_1 = m_filter.new("allpass", 142 * size, input_diff_1),
		allpass_2 = m_filter.new("allpass", 107 * size, input_diff_1),
		allpass_3 = m_filter.new("allpass", 379 * size, input_diff_2),
		allpass_4 = m_filter.new("allpass", 277 * size, input_diff_2),

		allpass_5a = m_filter.new("allpass", 672 * size, decay_diff_1),
		allpass_5b = m_filter.new("allpass", 908 * size, decay_diff_1),

		delay_1a = m_delay.new(4453 * size),
		delay_1b = m_delay.new(4217 * size),

		lowpass_2a = m_filter.new("lowpass", 1, damping),
		lowpass_2b = m_filter.new("lowpass", 1, damping),

		allpass_6a = m_filter.new("allpass", 1800 * size, decay_diff_2),
		allpass_6b = m_filter.new("allpass", 2656 * size, decay_diff_2),

		delay_2a = m_delay.new(3720 * size),
		delay_2b = m_delay.new(3163 * size),

		-- METHODS
		compute = m_reverb.compute
	}
end

m_reverb.compute =	function(self, x, y)
	local			delay_2a, delay_2b
	local			tank_a, tank_b
	local			factor
	local			acc

	x = (x + y) * 0.5

	-- DECORRELATION STAGE
	x = self.lowpass_1:compute(x)
	x = self.allpass_1:compute(x)
	x = self.allpass_2:compute(x)
	x = self.allpass_3:compute(x)
	x = self.allpass_4:compute(x)
	-- TANK
	delay_2a = self.delay_2a:get()
	delay_2b = self.delay_2b:get()
	tank_a = x + delay_2b * self.decay
	tank_b = x + delay_2a * self.decay
	--- TANK UP
	tank_a = self.allpass_5a:compute(tank_a)
	tank_a = self.delay_1a:compute(tank_a)
	tank_a = self.lowpass_2a:compute(tank_a)
	tank_a = self.allpass_6a:compute(tank_a * self.decay)
	self.delay_2a:compute(tank_a)
	--- TANK DOWN
	tank_b = self.allpass_5b:compute(tank_b)
	tank_b = self.delay_1b:compute(tank_b)
	tank_b = self.lowpass_2b:compute(tank_b)
	tank_b = self.allpass_6b:compute(tank_b * self.decay)
	self.delay_2b:compute(tank_b)

	-- OUTPUT
	factor = 0.6
	acc = factor * self.delay_1b:get(266)
	acc = acc + factor * self.delay_1b:get(2974)
	acc = acc - factor * self.allpass_6b.delay:get(1913)
	acc = acc + factor * self.delay_2b:get(1996)
	acc = acc + factor * self.delay_1a:get(1990)
	acc = acc - factor * self.allpass_5b.delay:get(187)
	x = acc - factor * self.delay_2a:get(1066)

	acc = factor * self.delay_1a:get(353)
	acc = acc + factor * self.delay_1a:get(3627)
	acc = acc - factor * self.allpass_6a.delay:get(1228)
	acc = acc + factor * self.delay_2a:get(2673)
	acc = acc + factor * self.delay_1b:get(2111)
	acc = acc - factor * self.allpass_5a.delay:get(353)
	y = acc - factor * self.delay_2b:get(121)

	return x, y
end

--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(48000)
	format("centered", 100)

	amp_max = 50
	p = 0
	wet = 0.7

	reverb_1 = m_reverb.new(0.9995,	-- bandwidth
	0.750, 0.625,					-- input diffusion 1 & 2 (0.750 & 0.625)
	0.7, 0.5,						-- decay diffusion 1 & 2 (0.7 & 0.5)
	0.5,							-- decay (0.5)
	0.0005,							-- damping (0.0005)
	4)								-- homemade room size
	reverb_2 = m_reverb.new(0.9995,	-- bandwidth
	0.750, 0.625,					-- input diffusion 1 & 2 (0.750 & 0.625)
	0.7, 0.5,						-- decay diffusion 1 & 2 (0.7 & 0.5)
	0.5,							-- decay (0.5)
	0.0005,							-- damping (0.0005)
	4)								-- homemade room size
end

function		update()
	local		notes = {freq("C4"), freq("d4"), freq("F4"), freq("G4")}
	local		note
	local		x, y
	local		wet_x, wet_y
	local		final_x, final_y
	local		amp
	local		t
	local		rev_point

	if time() > 12 then
		save("reverb-dattorro.wav")
	end

	note = notes[floor(1 + (time() * 3) % 4)]
	t = (time() * 3) % 1
	if t < 0.5 then
		amp = map(t, 0, 0.5, 0, amp_max, true)
	else
		amp = map(t, 0.5, 1, amp_max, 0, true)
	end
	p = p + note / 48000
	x = wave.tri(p, -amp, amp)
	y = wave.tri(p + 0.25, -amp, amp)

	-- REVERB 1
	wet_x, wet_y = reverb_1:compute(x, y)
	x = x * (1 - wet) + wet_x * wet
	y = y * (1 - wet) + wet_y * wet
	-- REVERB 2
	wet_x, wet_y = reverb_2:compute(x, y)
	final_x = x * (1 - wet) + wet_x * wet
	final_y = y * (1 - wet) + wet_y * wet
	point(final_x, final_y)
end
