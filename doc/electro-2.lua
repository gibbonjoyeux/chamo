
function		init()
	samplerate(44100)
	format("centered", 100)

	rects = {}
end

function		update()
	local		new_rects

	if time() > 10 then
		save("audio.wav")
	end

	-- FRAME
	for turn = 0, 2 do
		line(-99, -99, 99, -99, 50)
		line(99, -99, 99, 99, 50)
		line(99, 99, -99, 99, 50)
		line(-99, 99, -99, -99, 50)
	end

	--pointnoise(1)

	-- ADD RECT
	if #rects < 20 then
		table.insert(rects, 0.1)
	end
	-- UPDATE RECTS
	new_rects = {}
	for _, rect in pairs(rects) do
		scale(rect)
		line(-99, -99, 99, -99, 50)
		line(99, -99, 99, 99, 50)
		line(99, 99, -99, 99, 50)
		line(-99, 99, -99, -99, 50)
		rect = rect * 1.1
		if rect < 1 then
			table.insert(new_rects, rect)
		end
	end
	rects = new_rects
end
