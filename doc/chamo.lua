--[ GIBBON JOYEUX ]--

function		compute_notes()
	local		r
	local		note_octave
	local		note_new
	local		note
	local		i

	notes = {}
	for i = 1, 8 do
		note_new = notes_scale[ 1 + floor( rand( #notes_scale ) ) ]
		--while note_new == note do
		--	note_new = notes_scale[ 1 + floor( rand( #notes_scale ) ) ]
		--end
		note = note_new
		note_octave = note
		r = rand()
		--if r > 0.95 then
		--	note_octave = note_octave * 8
		--elseif r > 0.9 then
		--	note_octave = note_octave * 4
		--elseif r > 0.75 then
		--	note_octave = note_octave * 2
		--end
		notes[ i ] = note
	end
end

function		init()
	samplerate(48000)
	format("centered", 100)

	notes_scale = {freq('C2'), freq('d2'), freq('F2'), freq('G2'), freq('a2')}
	compute_notes()
	note_i = 1
	note_seq_i = 1
	--f = notes[ 1 ]
	--f_octave = f
	delay = 0
	t = 0
	chamo_scale = 1
	chamo_scale_aim = 1

	shape.begin()

		-- BACK
		shape.vertex(-30, 0)
		shape.vertex(-20, -20, -10, -20, 0, 0)
		shape.vertex(10, -20, 20, -20, 30, 0)

		-- NECK
		shape.vertex(35, 10, 44, 10, 45, 0)
		shape.vertex(50, -30, 60, -30, 65, -20)

		-- HEAD
		shape.vertex(80, -20, 80, -15)
		shape.vertex(80, -13, 78, -13)
		shape.vertex(80, -10, 70, -10)

		-- NECK
		shape.vertex(70, 35, 50, 35)

		-- LEG FRONT
		shape.vertex(40, 55, 50, 80)
		-- FOOT FRONT
		shape.vertex(60, 80, 60, 85, 58, 85)
		shape.vertex(40, 85)
		shape.vertex(39, 82, 40, 82, 42, 80)
		-- LEG FRONT
		shape.vertex(30, 60, 35, 35)

		-- BELLY
		shape.vertex(30, 50, -15, 50, -20, 35)

		-- LEG BACK
		shape.vertex(-20, 50, -35, 60)
		shape.vertex(-45, 65, -40, 80)
		-- FOOT BACK
		shape.vertex(-30, 80, -30, 85, -32, 85)
		shape.vertex(-50, 85)
		shape.vertex(-51, 82, -50, 82, -48, 80)
		-- LEG BACK
		shape.vertex(-52, 60, -50, 55, -45, 50)
		shape.vertex(-45, 40, -40, 30)
		shape.vertex(-40, 10, -30, 0)

	wave.chamo = shape.record()
end

function		update()
	local		x, y
	local		new_f
	local		r

	-- SEQUENCER
	if f == nil or time() > delay then
		--if rand() > 0.8 then
		--	delay = time() + 0.3
		--else
		--	delay = time() + 0.15
		--end

		delay = time() + 0.15

		-- FREQUENCY
		f = notes[ note_i ]
		note_i = note_i + 1
		if note_i > 8 then
			note_i = 1
			note_seq_i = note_seq_i + 1
			if note_seq_i > 4 then
				note_seq_i = 1
				compute_notes()
			end
		end
		-- OCTAVE
		r = rand()
		if r > 0.95 then
			f = f * 8
		elseif r > 0.9 then
			f = f * 4
		elseif r > 0.75 then
			f = f * 2
		end

		chamo_scale_aim = map(f,
		--[[--]] notes_scale[1], notes_scale[#notes_scale] * 8,
		--[[--]] 1, 0.05)
	end
	
	chamo_scale = lerp(0.001, chamo_scale, chamo_scale_aim)
	-- DRAWER
	--shape.draw(wave.chamo, 48000 / f_octave)
	x, y = shape.get( wave.chamo, t % 1 )
	rotate(time() * 0.1)
	scale(chamo_scale)
	point( x - 15, y - 15 )
	t = t + f / 48000

end
