--[ GIBBON JOYEUX ]--

function		init()
	samplerate(44100)
	mode("fps", 30)
	format("centered", 100)

	x = 50
	y = 0
	dirx = 1
	diry = 1
	speed = 10
	i = 0
end

function		update()
	local		r = 10

	-- SAVE FILE
	if time() >= 10 then
		save("audio.wav")
	end

	-- DRAW ROTATIN SQUARES
	matrix_push()
		for a = 0, 7 do
			rotate(i / 400)
			scale(0.9)
			line(-50, -50, -50, 50, 30)
			line(-50, 50, 50, 50, 30)
			line(50, 50, 50, -50, 30)
			line(50, -50, -50, -50, 30)
		end
	matrix_pop()

	-- DRAW TRIANGLE
	poly(0, 0, 20, 3, -0.082, 50, 1, 1)

	-- DRAW MOVING BALLS
	circle(x, y, r, 5, 10)
	circle(-x, y, r, 5, 10)
	circle(x, -y, r, 5, 10)
	circle(-x, -y, r, 5, 10)
	-- UPDATE MOVING BALLS
	x = x + dirx * speed
	y = y + diry * speed
	if x > 100 - r / 2 then dirx = -1
	elseif x < -100 + r / 2 then dirx = 1 end
	if y > 100 - r / 2 then diry = -1
	elseif y < -100 + r / 2 then diry = 1 end

	i = i + 1
end
