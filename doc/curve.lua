--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	p = 0
end

function		update()
	local		notes = {523.25, 659.25, 783.99, 659.25}
	local		note
	local		points

	note = notes[1 + floor(time() * 3) % #notes]
	points = 48000 / note
	-- CURVE TOP
	curve(-95, 0,
	--[[--]] -25 - sin(time()) * 20, -50 + sin(time()) * 20,
	--[[--]] 25 + sin(time()) * 20, -50 - sin(time()) * 20,
	--[[--]] 95, 0,
	--[[--]] points / 2)
	-- CURVE BOTTOM
	rotate(0.5)
	curve(-95, 0,
	--[[--]] -25 - sin(time()) * 20, -50 + sin(time()) * 20,
	--[[--]] 25 + sin(time()) * 20, -50 - sin(time()) * 20,
	--[[--]] 95, 0,
	--[[--]] points / 2)
end
