--[ GIBBON JOYEUX ]--

function		init()
	specs(44100, 24)
	format("centered", 100)
	i = 0
end

function		update()
	local		RADIUS	= 5
	local		MIN		= 80
	local		MAX		= 100
	local		points
	local		x, y, a
	local		angle
	local		amp
	local		t

	if time() >= 10 then
		save("audio.wav")
	end

	i = i + 1
	angle = time() * (48000 / freq('C4'))
	amp = map(noise(1, cos(angle) * RADIUS, sin(angle) * RADIUS,
	--[[--]] time() * 1), -1, 1, MIN, MAX)
	x = cos(angle) * amp
	y = sin(angle) * amp
	point(x, y)
end
