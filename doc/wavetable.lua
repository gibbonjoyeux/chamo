--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	table = {wave_1, wave_2}
	--table = {wave_1, wave_2, wave_2, wave_1}
	t = 0
	t1 = 0
	t2 = 0
end

function		update()
	t1 = t1 + 1 / (48000 / freq('C4'))
	t2 = wave.sin(time() * 1, 0, 1)
	scale(80)
	wave_table(t1, t2)
end

function		wave_table(t1, t2)
	local		step
	local		step_t
	local		wave_before, wave_after
	local		x1, y1, x2, y2
	local		x, y

	-- COMPUTE WAVETABLE
	step = 1 / (#table - 1)
	step_t = t2 * (#table - 1)
	wave_before = table[floor(step_t) + 1]
	wave_after = table[min(#table, ceil(step_t) + 1)]
	x1, y1 = wave_before(t1)
	x2, y2 = wave_after(t1)
	x = map(step_t % 1, 0, 1, x1, x2)
	y = map(step_t % 1, 0, 1, y1, y2)
	point(x, y)
end

function		wave_1(t)
	return wave.cos(t, -1, 1), wave.sin(t, -1, 1)
end

function		wave_2(t)
	return wave.tri(t, -1, 1), wave.tri(t + 0.25, -1, 1)
end

function		wave_3(t)
	return wave.saw(t, -1, 1), wave.saw(t + 0.5, -1, 1)
end
