
function		init()
	samplerate(44100)
	format("centered", 100)
	i = 0
end

function		update()
	if time() > 3 then
		save("audio.wav")
	end

	-- FRAME
	for turn = 0, 2 do
		line(-56, -99, 56, -99, 50)
		line(56, -99, 56, 99, 50)
		line(56, 99, -56, 99, 50)
		line(-56, 99, -56, -99, 50)
	end

	pointnoise(time() * 2)

	local	amp
	local	a
	local	x, y

	limit(time() * 1800 / 2)

	-- HALF CIRCLE LEFT
	amp = 13
	a = 0.4
	for i = 1, 180 do
		x = -20 + cos(a + i / 360) * amp
		y = sin(a + i / 360) * amp
		point(x, y, 1)
	end

	line(-12, -10, 7, 5, 100)

	-- HALF CIRCLE CENTER
	amp = 8
	a = -0.08
	--for i = 1, 180 do
	for i = 1, 240 do
		x = cos(a + i / 360) * amp
		y = 9 + sin(a + i / 360) * amp
		point(x, y, 1)
	end

	line(-7, 5, 12, -10, 100)

	-- HALF CIRCLE RIGHT
	amp = 13
	a = -0.4
	for i = 1, 180 do
		x = 20 + cos(a + i / 360) * amp
		y = sin(a + i / 360) * amp
		point(x, y, 1)
	end

	line(31, 7, 0, 40, 200)

	line(0, 40, -31, 7, 200)

	--if i == 0 then
	--	print(framecount())
	--end
	--i = i + 1
end
