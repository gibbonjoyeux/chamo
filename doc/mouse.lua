--[ GIBBON JOYEUX ]--

local	x, y
local	angle, angle_acc

function		init()
	specs(48000, 32)
	format("centered", 100)
	x = 0
	y = 0
	angle = 0
	angle_acc = 0
end

function		update()
	local		f
	local		vertices
	local		amp

	x = lerp( 0.1, x, mouse.x )
	y = lerp( 0.1, y, mouse.y )

	f = map(x, -90, 90, freq('C3'), freq('C6'), true)
	vertices = floor(map(y, -90, 90, 3, 16), true)

	if mouse.down == true then
		angle_acc = min(angle_acc + 0.00001, 0.005)
	elseif angle_acc > 0 then
		angle_acc = max(angle_acc - 0.00001, 0)
	end
	angle = angle + angle_acc

	polygon(x, y, 30, vertices, samplerate() / f, angle)
end

function		keydown( key )
	print(key)
end
