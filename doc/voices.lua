
--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(48000)
	format("centered", 100)
end

function		update()
	local		x1, y1, x2, y2, x3, y3, x4, y4, x5, y5
	local		t1, t2, t3, t4, t5
	local		x, y
	local		notes = {freq("C4"), freq("d4"), freq("F4"), freq("G4")}
	local		fre

	fre = notes[1 + floor(time() * 2) % 4]
	detune = 3

	t1 = time() * fre
	x1 = cos(t1)
	y1 = sin(t1)

	t2 = time() * (fre + detune)
	x2 = cos(t2)
	y2 = sin(t2)
	t3 = time() * (fre + detune * 2)
	x3 = cos(t3)
	y3 = sin(t3)

	t4 = time() * (fre - detune)
	x4 = cos(t2)
	y4 = sin(t2)
	t5 = time() * (fre - detune * 2)
	x5 = cos(t3)
	y5 = sin(t3)

	x = (x1 + x2 * 0.5 + x3 * 0.3 + x4 * 0.5 + x5 * 0.3) / 2.6
	y = (y1 + y2 * 0.5 + y3 * 0.3 + y4 * 0.5 + y5 * 0.3) / 2.6
	point(x * 50, y * 50)

	if time() > 4 then
		save("audio.wav")
	end

end
