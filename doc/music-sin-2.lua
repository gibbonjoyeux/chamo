--[ GIBBON JOYEUX ]--

function		init()
	samplerate(44100)
	format("centered", 100)

	i = 0
end

local function	put_note(freq, amp)
	x = cos(freq * i / 44100) * amp
	y = sin(freq * i / 44100) * amp
	point(x, y, 1)
end

function		update()
	local		notes = {523.25, 659.25, 783.99}
	local		t

	if time() >= 10 then
		save("audio.wav")
	end

	--- MULTI NOTES IN MANDALAS (continuous mode)
	t = math.floor(time()) % 3
	if t == 0 then
		put_note(notes[1] * 2, 20)
		put_note(notes[2] * 2, 40)
	elseif t == 1 then
		put_note(notes[1] * 2, 20)
		put_note(notes[3] * 2, 40)
	else
		put_note(notes[2] * 2, 20)
		put_note(notes[3] * 2, 40)
	end
	i = i + 1
end
