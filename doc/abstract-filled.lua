--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	notes = {freq("C3"), freq("d3"), freq("F3"), freq("G3")}
end

function		update()
	local		x, y
	local		fr

	fr = notes[1 + floor(time()) % 4]
	t = time() * fr / 2
	x = wave.saw(t, -100, 100)
	y = wave.rsaw(t + time() * 3, -100, 100)
	point(x, y)
	t = time() * fr
	x = wave.saw(t, -100, 100)
	y = wave.saw(t + time() * 2, -100, 100)
	point(x, y)

	if time() >= 4 then
		save("audio.wav")
	end
end
