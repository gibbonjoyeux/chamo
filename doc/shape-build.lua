--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	p = 0
end

function		update()
	--local		x, y
	--local		frequency

	--frequency = map(wave.sin(time() * 2), 0, 1, freq("C4"), freq("C1"))
	--p = p + frequency / 48000
	--x = wave.cos(p, -100, 100)
	--y = wave.sin(p, -100, 100)
	--point(x, y)

	local		frequency
	local		t

	t = map(sin(time()), -1, 1, -30, 50)
	--shape_begin()
	--	vertex(-30, -30)
	--	vertex(0, -30 - t, 30, -30)
	--	vertex(30 + t, 0, 30, 30)
	--	vertex(0, 30 + t, -30, 30)
	--	vertex(-30 - t, 0, -30, -30)
	--drawing = shape_record()
	--frequency = freq("C4")
	--shape(drawing, 48000 / frequency)

	shape.begin()
		shape.vertex(-30, -30)
		t = map(sin(time()), -1, 1, -30, 50)
		shape.vertex(0, -30 - t, 30, -30)
		t = map(sin(time() + 0.25), -1, 1, -30, 50)
		shape.vertex(30 + t, 0, 30, 30)
		t = map(sin(time() + 0), -1, 1, -30, 50)
		shape.vertex(0, 30 + t, -30, 30)
		t = map(sin(time() + 0.25), -1, 1, -30, 50)
		shape.vertex(-30 - t, 0, -30, -30)
	shape.complete(48000 / (freq('C4')))
end
