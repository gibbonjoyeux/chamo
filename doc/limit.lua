--[ GIBBON JOYEUX ]--

function		init()
	samplerate(44100)
	mode("fps", 30)
	format("centered", 100)

	i = 0
	prev_samples = 0
end

function		update()
	local		r
	local		samples

	-- SAVE FILE AFTER 30s
	if time() >= 30 then
		save("audio.wav")
	end

	-- SET LIMIT
	limit(i * 2)
	point_noise(0.8)

	-- ROTATING SQUARES
	matrix_push()
		for a = 0, 7 do
			rotate(i / 400)
			scale(0.9)
			line(-50, -50, -50, 50, 30)
			line(-50, 50, 50, 50, 30)
			line(50, 50, 50, -50, 30)
			line(50, -50, -50, -50, 30)
		end
	matrix_pop()

	-- TRIANGLE
	point_noise(0)
	poly(0, 0, 20, 3, 159, -0.082)

	-- OPTIONAL TRACING LINE
	--limit()
	--samples = framecount()
	--if samples ~= prev_samples then
	--	last_x, last_y  = lastpoint()
	--	line(last_x, last_y, 100, -100, 100)
	--end
	--prev_samples = samples

	i = i + 1
end
