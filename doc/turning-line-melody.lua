--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	bpm = 120
end

function		update()
	local		notes = {97, 194, 122, 194}

	point_noise(3);
	rotate(time() * 0.5 * (1 + floor(time_bpm(bpm) / 2) % #notes))
	line(-150, 100, 150, -100, notes[1 + floor(time_bpm(bpm)) % #notes])

	if time() >= #notes / 60 * bpm then
		save("audio.wav")
	end
end
