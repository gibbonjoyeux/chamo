--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
end

function		update()
	local		steps
	local		t
	local		x, y

	steps = floor(time() * 4) % 24
	t = time() * 440
	x = wave.strtri(t, steps, -90, 90)
	y = wave.strtri(t + 0.25, steps, -90, 90)
	point(x, y)

	if time() >= 10 then
		save("audio.wav")
	end
end
