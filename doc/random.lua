--[ GIBBON JOYEUX ]--

function		init()
	samplerate(44100)
	format("centered", 100)
end

function		update()
	local		x, y
	local		t

	if time() >= 10 then
		save("audio.wav")
	end

	t = time() * 4
	for a = 1, 360 do
		x = sin(a / 360) * 60 + rand(t) - t / 2
		y = cos(a / 360) * 60 + rand(t) - t / 2
		point(x, y, 1)
	end
	for a = 1, 360 do
		x = sin(a / 360) * 40 + rand(t) - t / 2
		y = cos(a / 360) * 40 + rand(t) - t / 2
		point(x, y, 1)
	end
	for a = 1, 360 do
		x = sin(a / 360) * 20 + rand(t) - t / 2
		y = cos(a / 360) * 20 + rand(t) - t / 2
		point(x, y, 1)
	end
end
