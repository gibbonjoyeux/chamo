
function		draw_b()
	line(0, 0, 0, 20, points)
	line(0, 20, 10, 15, points)
	line(10, 15, 0, 10, points)
	line(0, 10, 10, 5, points)
	line(10, 5, 0, 0, points)
end

function		draw_m()
	line(0, 20, 0, 0, points)
	line(0, 0, 10, 20, points)
	line(10, 20, 20, 0, points)
	line(20, 0, 20, 20, points)
end

function		draw_y()
	line(10, 0, 0, 20, points * 2)
	line(5, 10, 0, 0, points)
end

function		draw_s()
	line(10, 0, 0, 7, points)
	line(0, 7, 10, 14, points)
	line(10, 14, 0, 20, points)
end

function		draw_c()
	line(10, 0, 0, 0, points)
	line(0, 0, 0, 20, points)
	line(0, 20, 10, 20, points)
end

function		draw_o()
	line(5, 0, 10, 10, points)
	line(10, 10, 5, 20, points)
	line(5, 20, 0, 10, points)
	line(0, 10, 5, 0, points)
end

function		draw_n()
	line(0, 20, 0, 0, points)
	line(0, 0, 10, 20, points)
	line(10, 20, 10, 0, points)
end

function		draw_u()
	line(0, 0, 0, 20, points)
	line(0, 20, 10, 20, points)
	line(10, 20, 10, 0, points)
end

function		draw_e()
	line(10, 0, 0, 0, points)
	line(0, 0, 0, 10, points)
	line(0, 10, 5, 10, points / 2, 2)
	line(0, 10, 0, 20, points)
	line(0, 20, 10, 20, points)
end

function		draw_buy(x, y, s, a)
	pushmatrix()
		translate(x, y)
		if s then
			scale(s)
		end
		if a then
			rotate(a)
		end
		-- B
		draw_b()
		-- U
		translate(15, 0); draw_u()
		-- Y
		translate(15, 0); draw_y()
	popmatrix()
end

function		draw_consume(x, y, s, a)
	pushmatrix()
		translate(x, y)
		if s then
			scale(s)
		end
		if a then
			rotate(a)
		end
		-- C
		draw_c()
		-- O
		translate(15, 0); draw_o()
		-- N
		translate(15, 0); draw_n()
		-- S
		translate(15, 0); draw_s()
		-- U
		translate(15, 0); draw_u()
		-- M
		translate(15, 0); draw_m()
		-- E
		translate(20, 0); draw_e()
	popmatrix()
end

function		init()
	samplerate(44100)
	format("centered", 100)

	points = 20
	i = 0
	--prev_samples = 0
end

function		update()
	local		new_rects
	local		j

	if time() > 10 then
		save("audio.wav")
	end

	-- FRAME
	for turn = 0, 2 do
		line(-56, -99, 56, -99, 50)
		line(56, -99, 56, 99, 50)
		line(56, 99, -56, 99, 50)
		line(-56, 99, -56, -99, 50)
	end

	-- LINES
	local	turns = 16
	local	x1, y1
	local	x2, y2
	local	len
	local	amp

	pointnoise(3)
	amp = ((time() * 2) % 1) * 100
	if amp < 28 then
		len = map(amp, 0, 28, 0, 30)
	else
		len = map(amp, 28, 56, 30, 0)
	end
	for a = 0, turns - 1 do
		x1 = constrain(cos(a /turns) * amp, -56, 56)
		y1 = constrain(sin(a / turns) * amp, -99, 99)
		x2 = constrain(cos(a /turns) * (amp - len), -56, 56)
		y2 = constrain(sin(a / turns) * (amp - len), -99, 99)
		line(x1, y1, x2, y2, 10)
	end

	-- TEXT
	pointnoise(1)
	points = map((time() * 2) % 1, 0, 1, 0, 20)
	draw_buy(-18, -10, 1)
end
