--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	bpm = 150
end

function		update()
	local		notes = {freq("C2"), freq("d2"), freq("E2"), freq("F2")}
	local		note_freq

	rotate(time())
	scale(time_bpm(bpm) % 1)
	point_noise(map(time_bpm(bpm) % 1, 0, 1, 10, 0))

	note_freq = notes[1 + floor(time_bpm(bpm)) % #notes]

	fpoly({
		-50, -50, 50, -50, 50, 50, -40, 50,
		-40, -40, 40, -40, 40, 40, -30, 40,
		-30, -30, 30, -30, 30, 30, -20, 30,
		-20, -20, 20, -20, 20, 20, -10, 20,
		-10, -10, 10, -10, 10, 10, 0, 10,
		0, 0,

		0, 10, 10, 10, 10, -10, -10, -10,
		-10, 20, 20, 20, 20, -20, -20, -20,
		-20, 30, 30, 30, 30, -30, -30, -30,
		-30, 40, 40, 40, 40, -40, -40, -40,
		-40, 50, 50, 50, 50, -50, -50, -50,
		}, 48000 / note_freq)

	if time() >= 10 then
		save("audio.wav")
	end
end
