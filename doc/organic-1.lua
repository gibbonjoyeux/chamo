--[ GIBBON JOYEUX ]--

function		init()
	samplerate(44100)
	format("centered", 100)
	i = 0
end

function		update()

	if time() >= 10 then
		save("audio.wav")
	end

	local	s

	-- COMPUTE SCALE
	s = 1
	+ wave.sin(time() * 440 * 10 - i / 100, 0, 0.1)
	+ wave.cos(time() * 440 * 5 + i / 100, 0, 0.1)
	+ wave.cos(time() * 440 * 40 + i / 20, 0, 0.01)
	-- ROTATE WITH 440 FREQ
	rotate(time() * 440)
	-- DRAW 1st POINT
	push()
		scale(s - wave.sin(time() / 3, 0, 0.3))
		point(50, 0)
	pop()
	-- DRAW 2nd POINT
	push()
		scale(s)
		point(70, 0)
	pop()
	-- UPDATE i
	i = i + 1
end
