
function		module_sin(params)
	local		note
	local		fre

	note = params[1]
	fre = freq(note)
	matrix_push()
		rotate(time() * fre)
		point(99, 0)
	matrix_pop()
end

function		module_squ(params)
	local		note
	local		fre

	note = params[1]
	fre = freq(note)
	matrix_push()
		rotate(time() * fre)
		point(55 + sin(time() * 4) * 20, 0)
	matrix_pop()
end

track = {
	modules = {	{module_sin, 1},	{module_squ, 1}}, -- {module_function, number_of_parameters}
	tracks = {
				-- n	= Start module
				-- 0	= Continue module
				-- -1	= Stop module
				{1, "C4"}, {0},
				{1, "d4"}, {0},
				{1, "F4"}, {0},
				{1, "G4"}, {0},
				{1, "C4"}, {2, "C5"},
				{1, "d4"}, {2, "d5"},
				{1, "F4"}, {2, "F5"},
				{1, "G4"}, {2, "G5"}
	}
}

function		read_tracker(track, lpm)
	local		line
	local		line_state
	local		module, next_module, origin_module
	local		module_func
	local		params
	local		ret

	line_state = (time() * lpm) % 1
	line = 1 + floor(time() * lpm)
	module = track.tracks[line] and track.tracks[line][1]
	next_module = track.tracks[line + 1] and track.tracks[line + 1][1]

	if module == 0 then
		origin_module = module
		module = track.prev_module or 0
		line = track.prev_line or 1
	else
		track.prev_module = module
		track.prev_line = line
	end
	--print(module, next_module)

	if module == nil then
		save("audio.wav")
	elseif module == -1 then
		point(0, 0)
		print("bug")
	else
		module_func = track.modules[module][1]
		params = {}
		for i = 1, track.modules[module][2] do
			table.insert(params, track.tracks[line][1 + i])
		end
		matrix_push()
			if next_module ~= 0 then
				if line_state > 0.99 then
					scale(map(line_state, 0.99, 1, 1, 0))
				end
			end
			if origin_module ~= 0 then
				if line_state < 0.01 then
					scale(map(line_state, 0, 0.01, 0, 1))
				end
			end
			ret = module_func(params, prev_ret)
			-- TODO: use ret to allow modification through time
		matrix_pop()
	end
end

--------------------------------------------------------------------------------
-- MAIN
--------------------------------------------------------------------------------

function		init()
	samplerate(48000)
	format("centered", 100)

	lpm = 1	-- lines per minute
end

function		update()

	read_tracker(track, lpm)

end
