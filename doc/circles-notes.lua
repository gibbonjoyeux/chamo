--[ GIBBON JOYEUX ]--

function		init()
	samplerate(48000)
	format("centered", 100)
	bpm = 120
end

function		update()
	local		t

	t = min(3, floor(time_bpm(bpm)))

	for i = 0, t do
		circle(0, 0, 100 - i * 10, 48000 / 220)
	end

	if time() >= 10 then
		save("audio.wav")
	end
end
