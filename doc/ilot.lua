
function		init()
	samplerate(44100)
	format("centered", 100)
	points = 30
end

function		update()

	if time() > 1 then
		save("audio.wav")
	end

	translate(-32, -15)

	matrix_push()
		point_noise(3)
		-- I base
		line(0, 0, 0, 40, points * 3)
		line(10, 0, 10, 10, points)
		line(10, 15, 10, 30, points)
		line(10, 35, 10, 40, points)
		line(20, 0, 20, 40, points * 3)
		-- I hat
		line(0, -5, 10, -10, points)
		line(10, -10, 20, -5, points)
		-- L
		translate(30, 0)
		line(0, 0, 0, 40, points * 3)
		line(0, 40, 10, 40, points)
		-- O
		translate(15, 0)
		line(0, 0, 0, 40, points * 3)
		line(0, 40, 10, 40, points)
		line(10, 40, 10, 0, points * 3)
		line(10, 0, 0, 0, points)
		-- T
		translate(15, 0)
		line(0, 0, 10, 0, points)
		line(5, 0, 5, 45, points * 3)
	matrix_pop()

	-- FRAME
	line(-20, -17, 85, -17, 150)
	line(85, -17, 85, 50, 100)
	line(85, 50, -20, 50, 150)
	line(-20, 50, -20, -17, 100)
end
