/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef CHAMO_H
# define CHAMO_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/stat.h>

#include <pthread.h>

#include <lauxlib.h>
#include <luajit.h>
#include <lualib.h>

#include <SDL2/SDL.h>

#include "basics.h"
#include "string.h"
#include "lst.h"
#include "vec.h"

#include "wave.h"

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// DEFINES
//////////////////////////////////////////////////

#define	PI							((double)3.141592653)
#define PI2							((double)6.28318507)
#define	true						1
#define false						0

#define DEFAULT_SAMPLERATE			48000
#define DEFAULT_BITSPERSAMPLE		32

#define CHUNK_LEN					1//5		// Chunk length in seconds
#define CHUNK_NUM_SAMPLES			(CHUNK_LEN * (size_t)w.header.sampleRate)	// Chunk samples
#define CHUNK_SIZE					(CHUNK_NUM_SAMPLES * sizeof(double) * 2)	// Chunk byte num

#define MODE_LIVE					0
#define MODE_SAVE					1
#define FORMAT_CORNER				0
#define FORMAT_CENTERED				1
#define FORMAT_SOUND				2
#define DEFAULT_FORMAT_MODE			FORMAT_SOUND
#define DEFAULT_FORMAT_WIDTH		2
#define DRAW_VECTOR					0
#define DRAW_TIME					1
#define DEFAULT_DRAW_MODE			DRAW_VECTOR

#define LIVE_SDL_SAMPLES			1024
#define LIVE_DEFAULT_BPS			32			// 32 bits per samples (float)

#define DRAW_TIME_MAX				96000

#define ENDIAN_BIG(data)			if (c.endian == false) \
									{ endian_reverse(&data, sizeof(data)); }
#define ENDIAN_LITTLE(data)			if (c.endian == true) \
									{ endian_reverse(&data, sizeof(data)); }

//////////////////////////////////////////////////
/// TYPEDEFS
//////////////////////////////////////////////////

typedef struct sockaddr_storage	t_sockaddr_storage;
typedef struct sockaddr_in		t_sockaddr_in;
typedef struct sockaddr			t_sockaddr;

typedef struct s_noise			t_noise;
typedef struct s_chamo			t_chamo;

//////////////////////////////////////////////////
/// STRUCTURES
//////////////////////////////////////////////////

struct						s_noise {
	int16_t					perm[256];
	int16_t					permGradIndex3D[256];
};

struct						s_chamo {
	char					mode;				// mode: LIVE | SAVE

	SDL_Window				*window;			// SDL Window
	SDL_Renderer			*renderer;			// SDL Window renderer

	struct					{
		SDL_AudioDeviceID	audio_id;
		t_list				*chunk;				// Current playing chunk
		size_t				play_index;			// Playing chunk sample index
		double				last_point_x;		// Last drawn point x coord
		double				last_point_y;		// Last drawn point y coord
	}						live;

	struct					{
		int					socket;
		t_sockaddr_in		server;
	}						osc;

	t_noise					noise;				// Open simplex noise structure
	bool					endian;				// Endian big or little (1 or 0)

	pthread_mutex_t			mutex;
	bool					quit;				// Quit boolean
};

//////////////////////////////////////////////////
/// STRUCTURES
//////////////////////////////////////////////////

lua_State			*l;
t_wave				w;
t_chamo				c;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void	init_sdl(void);
void	init_sdl_audio(void);
void	init_lua(char *file_path);
void	init_wave_struct(void);
void	init_chamo_struct(void);

void	close_sdl(void);
void	close_sdl_audio(void);
void	close_lua(void);
void	close_wave_struct(void);

void	reset_env(void);
void	save_module(int argc, char **argv);
void	play_module(char *file_path);
void	compile_module_turn(void);

void	live_send_samples(float *buffer, size_t len);

double	note_to_freq(char *note);

bool	endian_is_big(void);
void	endian_reverse(void *data, int len);
void	open_simplex_noise_init(int64_t seed);
double	open_simplex_noise2(double x, double y);
double	open_simplex_noise3(double x, double y, double z);
double	open_simplex_noise4(double x, double y, double z, double w);

//////////////////////////////////////////////////
/// EASE FUNCTIONS
//////////////////////////////////////////////////

double	ease_linear(double x);
double	ease_quad(double x);
double	ease_quad_in(double x);
double	ease_quad_out(double x);
double	ease_quart(double x);
double	ease_quart_in(double x);
double	ease_quart_out(double x);
double	ease_quint(double x);
double	ease_quint_in(double x);
double	ease_quint_out(double x);
double	ease_exp(double x);
double	ease_exp_in(double x);
double	ease_exp_out(double x);
double	ease_cubic(double x);
double	ease_cubic_in(double x);
double	ease_cubic_out(double x);
double	ease_sine(double x);
double	ease_sine_in(double x);
double	ease_sine_out(double x);
double	ease_elastic(double x);
double	ease_elastic_in(double x);
double	ease_elastic_out(double x);
double	ease_circ(double x);
double	ease_circ_in(double x);
double	ease_circ_out(double x);
double	ease_back(double x);
double	ease_back_in(double x);
double	ease_back_out(double x);
double	ease_bounce(double x);
double	ease_bounce_in(double x);
double	ease_bounce_out(double x);

#endif

