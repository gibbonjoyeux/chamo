/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef TRACKER_H
# define TRACKER_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// DEFINES
//////////////////////////////////////////////////

#define CELL_STOP			0		// Cell stop
#define CELL_KEEP			1		// Cell keep (previous cells)
#define CELL_CONTENT		2		// Cell content

#define TRACK_VAL_NUM		0		// Value is a number
#define TRACK_VAL_STR		1		// Value is a string
#define TRACK_VAL_VAR		2		// Value is a variable
#define TRACK_VAL_OPE		3		// Value is an operation

#define NUM_DEFAULT_EASE	32		// Number of chamo made easing functions

#define TRACKER_DEFAULT_BPM	135		// Beats per minute
#define TRACKER_DEFAULT_BPS	4		// Beats per stave
#define TRACKER_DEFAULT_LPB	4		// Lines per beat

//////////////////////////////////////////////////
/// TYPEDEFS
//////////////////////////////////////////////////

/// DYNAMIC - COMPILED
typedef struct s_track_compile	t_track_compile;	// Compilation structure
typedef struct s_track_comp_cel	t_track_comp_cel;	// Compiled cell
typedef struct s_track_comp_evt	t_track_comp_evt;	// Compiled event
typedef struct s_track_comp_var	t_track_comp_var;	// Compiled variable
typedef struct s_track_comp_mod	t_track_comp_mod;	// Compiled module
/// STATIC - PATTERN
typedef struct s_track_cel		t_track_cel;		// Track cell
typedef struct s_track_var		t_track_var;		// Track variable
typedef struct s_track_evt		t_track_evt;		// Track event
typedef struct s_track_mod		t_track_mod;		// Track module
typedef struct s_track_fct		t_track_fct;		// Track function (easing)
typedef struct s_track_pat		t_track_pat;		// Track pattern
/// STATIC - SOURCE
typedef struct s_track_src_mod	t_track_src_mod;	// Source module
typedef struct s_track_src_var	t_track_src_var;	// Source variable
typedef struct s_track_src_fct	t_track_src_fct;	// Source function (easing)
typedef struct s_track_source	t_track_source;		// Project source
/// MAIN
typedef struct s_track			t_track;			// Main tracker structure
typedef struct s_track_value	t_track_value;		// Tracker basic value

//////////////////////////////////////////////////
/// STRUCTURES
//////////////////////////////////////////////////

struct					s_track_value {
	unsigned int		type		: 2;	// 0: num | 1: str | 2: var | 3: ope
	union				{
		double			num;				// Value as number
		char			*str;				// Value as string
		uint16_t		var;				// Value as variable
		char			*ope;				// Value as operation string
	}					value;
};

//////////////////////////////
/// DYNAMIC DATA (COMPILED)
//////////////////////////////

struct					s_track_comp_evt {
	double				line;				// Event line
	uint8_t				fct;				// Event function (easing)
	t_track_value		value;				// Event value
};

struct					s_track_comp_var {
	uint16_t			source_id;			// Variable source variable id
	uint32_t			num_events;			// Variable events number
	t_track_comp_evt	*events;			// Variable events
	t_track_value		value;				// Variable current value
};

struct					s_track_comp_mod {
	uint16_t			source_id;			// Module source modules id
	uint16_t			num_inputs;			// Number of input modules
	double				value;				// Module current value
	t_track_comp_mod	**inputs;			// Module inputs
};

struct					s_track_comp_cel {
	uint32_t			num_lines;			// Cell total number of lines
	uint32_t			line;				// Cell current line
	uint32_t			sample;				// Cell current sample

	uint8_t				num_params;			// Number of parameters
	t_track_value		*params;			// Cell parameters

	t_list				*chunks;			// Samples chunks
	uint32_t			chunk_index;		// Current chunk index
	uint32_t			chunk_read_index;	// Last chunk reading index
	double				last_point[2];		// Last drawn point
};

struct					s_track_compile {
	uint32_t			num_lines;			// Track number of lines
	uint32_t			line;				// Current line index
	//uint32_t			line_sample;		// Current line sample index
	double				line_state;			// Current line progress
	uint32_t			sample;				// Total sample index
	uint32_t			samples_per_line;	// Samples per line

	t_track_comp_cel	***cells;			// Live track cells array
	t_track_comp_var	*variables;			// Track variables
	t_track_comp_mod	*modules;			// Track variables
};

//////////////////////////////
/// STATIC DATA (INTERFACE)
//////////////////////////////

////////////////////
/// PATTERN
////////////////////

struct					s_track_cel {
	char				*content;			// Cell content
	uint8_t				type		: 2;	// Cell type: 0: empty | 1: keep | 2: content
};

struct					s_track_evt {
	double				line;				// Event line
	uint8_t				fct;				// Event function (easing)
	t_track_value		value;				// Event value
};

struct					s_track_var {
	uint16_t			source_id;			// Variable source variable id
	t_list				*events;			// Variable events
};

struct					s_track_mod {
	uint16_t			source_id;			// Module source module id
	t_track_cel			*cells;				// Module cells
};

struct					s_track_pat {
	uint8_t				row;				// Pattern row position (track)
	uint32_t			line;				// Pattern line position (track)
	uint32_t			line_from;			// Pattern active slice start (track)
	uint32_t			line_to;			// Pattern active slice end (track)
	uint32_t			num_lines;			// Number of pattern lines
	uint16_t			num_modules;		// Number of pattern active modules
	uint16_t			num_variables;		// Number of pattern active variables
	t_track_mod			*modules;			// Pattern modules
	t_track_var			*variables;			// Pattern variables
};

////////////////////
/// SOURCES
////////////////////

struct					s_track_src_fct {
	char				*name;				// Ease name
	uint8_t				id;					// Ease id
	bool				user;				// Is fct user module or default
	union				{					// Ease function
		double			(*ptr)(double);		// Function pointer (C, default)
		char			*path;				// Function path (lua module, user)
	}					func;
};

struct					s_track_src_var {
	char				*name;				// Source variable name
	uint16_t			id;					// Source variable id
};

struct					s_track_src_mod {
	char				*name;				// Source module name
	uint16_t			id;					// Source module id
	uint16_t			source_id;			// Source module source id
	uint16_t			num_outputs;		// Number of output modules
	t_track_src_mod		**outputs;			// Output modules
};

struct					s_track_source {
	char				*path;				// Source path
	uint16_t			id;					// Source id
};

//////////////////////////////
/// MAIN
//////////////////////////////

struct					s_track {
	uint16_t			num_sources;		// Number of sources
	uint16_t			num_modules;		// Number of modules
	uint16_t			num_variables;		// Number of variables (events)
	uint8_t				num_functions;		// Number of functions (easing)
	uint32_t			num_patterns;		// Number of patterns

	t_track_source		*sources;			// Sources
	t_track_src_mod		*modules;			// Modules & path
	t_track_src_var		*variables;			// Variables events
	t_track_src_fct		*functions;			// Functions modules & path (easing)
	t_track_pat			*patterns;			// Track patterns

	t_track_compile		compile;			// Compile structure

	lua_State			*l;					// Operations lua state

	struct				{
		//uint16_t		bpm;				// Beats per minute
		double			bpm;				// Beats per minute
		uint8_t			bps;				// Beats per stave
		uint8_t			lpb;				// Lines per beat
	}					time;
};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//void		init_tracker(void);
//// > void		close_tracker(void);
///// MODULES
//bool		tracker_add_module(void);
//bool		tracker_del_module(uint16_t index);
//bool		tracker_set_module_name(uint16_t index, char *name);
//bool		tracker_set_module_path(uint16_t index, char *path);
//void		tracker_swap_modules(uint16_t index_1, uint16_t index_2);
///// TRACK LINES & CELLS
//bool		tracker_add_lines(uint32_t index, uint32_t number);
//bool		tracker_del_lines(uint32_t index, uint32_t number);
//bool		tracker_set_cell(uint16_t module, uint32_t line, uint8_t type,
//			char *content);
///// VARIABLES
//bool		tracker_add_variable(void);
//bool		tracker_del_variable(uint16_t index);
//bool		tracker_set_variable_name(uint16_t index, char *name);
//void		tracker_swap_variables(uint16_t index_1, uint16_t index_2);
///// VARIABLES EVENTS
//bool		tracker_add_event(uint16_t var_index, uint32_t event_index);
//bool		tracker_del_event(uint16_t var_index, uint32_t event_index);
//void		tracker_set_event_num(uint16_t var_index, uint32_t evt_index, double number);
//bool		tracker_set_event_str(uint16_t var_index, uint32_t evt_index, char *string);
//void		tracker_set_event_line(uint16_t var_index, uint32_t evt_index, uint32_t line);
//void		tracker_set_event_ease(uint16_t var_index, uint32_t evt_index, uint8_t ease);
//void		tracker_swap_events(uint16_t var_index, uint32_t index_1, uint32_t index_2);
///// EVENTS EASING
//bool		tracker_add_default_ease(char *name, double (*func)(double));
//bool		tracker_add_user_ease(void);
//bool		tracker_del_user_ease(uint8_t ease_index);
//bool		tracker_set_user_ease_name(uint8_t ease_index, char *name);
//bool		tracker_set_user_ease_path(uint8_t ease_index, char *path);
///// TRACK VALUE
//void		tracker_init_value(t_track_value *value);
//bool		tracker_set_value(t_track_value *value, char *str);
//void		tracker_reset_value(t_track_value *value);
//bool		tracker_push_value(lua_State *lua_state, t_track_value *value);
//
//bool		tracker_compile_init(uint32_t from, uint32_t to);
//bool		tracker_compile_turn(void);
//bool		tracker_compile_variables(void);
//
//void		tracker_print(void);

#endif
