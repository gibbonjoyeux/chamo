/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef WAVE_H
# define WAVE_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

#define VERTEX_LINEAR			0
#define VERTEX_QUADRATIC		1
#define VERTEX_CUBIC			2
#define VERTEX_ARC				3

#define SHAPE_DEFAULT_PRECISION	5

//////////////////////////////////////////////////
/// TYPEDEFS
//////////////////////////////////////////////////

typedef lua_Integer				lint;

typedef struct s_vec			t_vec;

typedef struct s_pool			t_pool;

typedef struct s_vertex_quad	t_vertex_quad;
typedef struct s_vertex_cubic	t_vertex_cubic;
typedef struct s_vertex_arc		t_vertex_arc;
typedef struct s_vertex_base	t_vertex_base;
typedef union  u_vertex_extra	t_vertex_extra;
typedef struct s_vertex			t_vertex;
typedef struct s_shape			t_shape;
typedef struct s_shape_user		t_shape_user;

typedef struct s_matrix			t_matrix;
typedef struct s_wave_header	t_wave_header;
typedef struct s_wave			t_wave;

//////////////////////////////////////////////////
/// STRUCTURES
//////////////////////////////////////////////////

struct				s_vec {
	double			x;
	double			y;
};

//////////////////////////////
/// HEADER
//////////////////////////////

struct				s_wave_header {
	// Riff Wave Header
	char			chunkId[4];
	int				chunkSize;
	char			format[4];
	// Format Subchunk
	char			subChunk1Id[4];
	int				subChunk1Size;
	short int		audioFormat;
	short int		numChannels;
	int				sampleRate;
	int				byteRate;
	short int		blockAlign;
	short int		bitsPerSample;
	//short int extraParamSize;
	// Data Subchunk
	char			subChunk2Id[4];
	int				subChunk2Size;

};

//////////////////////////////
/// POOL
//////////////////////////////

struct				s_pool {
	size_t			item_size;
	t_list			*stack;
	t_list			*pool;
	t_list			*stack_last;
};

//////////////////////////////
/// SHAPE
//////////////////////////////

struct				s_vertex_quad {
	t_vec			controller_1;
};

struct				s_vertex_cubic {
	t_vec			controller_1;
	t_vec			controller_2;
};

struct				s_vertex_arc {
	t_vec			center;
	t_vec			radius;
	double			angle;
	int				direction : 1;
};

struct				s_vertex_base {
	uint8_t			type;
	double			length;
	t_vec			coord;
};

union				u_vertex_extra {
	t_vertex_quad	quad;
	t_vertex_cubic	cubic;
	t_vertex_arc	arc;
};

struct				s_vertex {
	t_vertex_base	base;
	t_vertex_extra	extra;
};

struct				s_shape {
	double			length;
	size_t			num_vertices;
	size_t			alloc_size;
	uint8_t			precision;
	t_pool			vertices;
	t_vec			*last_point;
	t_vec			*first_point;
};

struct				s_shape_user {
	size_t			num_vertices;
	t_vertex		*vertices;
};

//////////////////////////////
/// WAVE
//////////////////////////////

struct				s_matrix {
	t_vec			coord;
	double			scale;
	double			angle;
};

struct				s_wave {
	t_wave_header	header;

	struct			{
		t_vec		last;				// Last drawn point

		t_pool		chunks;				// Chunk stack & pool
		uint32_t	chunk_index;		// sample index in current chunk

		uint32_t	count;				// total sample count
		uint32_t	count_sent;			// total sent sample count (sdl)

		bool		limit_set;			// is limit set
		uint32_t	limit_count;		// limit count until stop
	}				samples;

	struct			{
		char		mode;				// Format mode: CLASSIC | CENTERED | SOUND
		double		width;				// Format width
		t_vec		map_from;			// Format left / top value
		t_vec		map_to;				// Format right / bottom value
	}				format;

	struct			{
		double		noise;				// point_noise() value
		int			wait;				// point_wait() value
	}				point;

	t_shape			shape;				// Shape building
	t_matrix		matrix;				// Base coordinates matrix
	t_pool			matrices;			// Matrix stack & pool
};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// WAVE CREATION
//////////////////////////////////////////////////

void	wave_set_specs(int sample_rate, short int bits_per_sample);
bool	wave_add_sample(t_vec sample);
void	wave_new_chunk(void);
void	wave_save(char *path);

void	to_big_endian(long long int size, void *value);
void	to_little_endian(long long int size, void *value);

//////////////////////////////////////////////////
/// DRAWING
//////////////////////////////////////////////////

void	draw_point(t_vec p);
void	draw_circle(t_vec center, double r, lint points);
void	draw_line(t_vec p0, t_vec p1, lint points);
void	draw_bezier_quad(t_vec p0, t_vec p1, t_vec p2, lint points);
void	draw_bezier_cubic(t_vec p0, t_vec p1, t_vec p2, t_vec p3, lint points);
void	draw_polygon(t_vec center, double r, lint vertices, lint points,
		double angle);

void	apply_matrix_point(t_vec *point);
void	apply_matrix_dim(double *dim);
void	apply_matrix_angle(double *angle);
void	reset_matrix(void);

//////////////////////////////////////////////////
/// TOOLS
//////////////////////////////////////////////////

void	pool_init(t_pool *pool, size_t item_size);
t_list	*pool_push_item(t_pool *pool);
t_list	*pool_append_item(t_pool *pool);
t_list	*pool_pop_item(t_pool *pool);
void	pool_reset(t_pool *pool);
void	pool_free(t_pool *pool);

//////////////////////////////////////////////////
/// LUA
//////////////////////////////////////////////////

void	init_format(int mode, double width);
void	init_global_functions(void);
void	init_global_tables(void);
lint	get_param_int(int index);
double	get_param_float(int index);
t_vec	get_param_coord(int index);
double	get_param_dim(int index);

//////////////////////////////////////////////////
/// API
//////////////////////////////////////////////////

//////////////////////////////
/// MODE & FORMAT
//////////////////////////////

int		api_specs(lua_State *l);
int		api_format(lua_State *l);

int		api_save(lua_State *l);
int		api_exit(lua_State *l);

//////////////////////////////
/// TIME
//////////////////////////////

int		api_time(lua_State *l);
int		api_time_bpm(lua_State *l);
int		api_samplerate(lua_State *l);
int		api_samplecount(lua_State *l);

//////////////////////////////
/// MATH
//////////////////////////////

int		api_cos(lua_State *l);
int		api_sin(lua_State *l);
int		api_tan(lua_State *l);
int		api_acos(lua_State *l);
int		api_asin(lua_State *l);
int		api_atan(lua_State *l);
int		api_atan2(lua_State *l);
int		api_min(lua_State *l);
int		api_max(lua_State *l);
int		api_constrain(lua_State *l);
int		api_map(lua_State *l);
int		api_lerp(lua_State *l);
int		api_rand(lua_State *l);
int		api_floor(lua_State *l);
int		api_ceil(lua_State *l);
int		api_round(lua_State *l);
int		api_noise(lua_State *l);

//////////////////////////////
/// DRAW
//////////////////////////////

int		api_draw_wait(lua_State *l);
int		api_draw_point(lua_State *l);
int		api_draw_circle(lua_State *l);
int		api_draw_line(lua_State *l);
int		api_draw_curve(lua_State *l);
int		api_draw_polygon(lua_State *l);

int		api_matrix_translate(lua_State *l);
int		api_matrix_scale(lua_State *l);
int		api_matrix_rotate(lua_State *l);
int		api_matrix_push(lua_State *l);
int		api_matrix_pop(lua_State *l);

int		api_shape_begin(lua_State *l);
int		api_shape_complete(lua_State *l);
int		api_shape_record(lua_State *l);
int		api_shape_vertex(lua_State *l);
int		api_shape_draw(lua_State *l);
int		api_shape_get(lua_State *l);

int		api_limit(lua_State *l);
int		api_lastpoint(lua_State *l);

int		api_pointnoise(lua_State *l);
int		api_pointwait(lua_State *l);

int		api_freq(lua_State *l);

//////////////////////////////
/// WAVE
//////////////////////////////

double	api_wave_remap(lua_State *l, double value, int index);

int		api_wave_sin(lua_State *l);
int		api_wave_cos(lua_State *l);
int		api_wave_tri(lua_State *l);
int		api_wave_squ(lua_State *l);
int		api_wave_saw(lua_State *l);
int		api_wave_rsaw(lua_State *l);
int		api_wave_pul(lua_State *l);
int		api_wave_asin(lua_State *l);
int		api_wave_hsin(lua_State *l);
int		api_wave_strsaw(lua_State *l);
int		api_wave_strtri(lua_State *l);

//////////////////////////////
/// EASING
//////////////////////////////

int		api_ease_linear(lua_State *l);
int		api_ease_sine_in(lua_State *l);
int		api_ease_sine_out(lua_State *l);
int		api_ease_sine(lua_State *l);
int		api_ease_quad_in(lua_State *l);
int		api_ease_quad_out(lua_State *l);
int		api_ease_quad(lua_State *l);
int		api_ease_cubic_in(lua_State *l);
int		api_ease_cubic_out(lua_State *l);
int		api_ease_cubic(lua_State *l);
int		api_ease_quart_in(lua_State *l);
int		api_ease_quart_out(lua_State *l);
int		api_ease_quart(lua_State *l);
int		api_ease_quint_in(lua_State *l);
int		api_ease_quint_out(lua_State *l);
int		api_ease_quint(lua_State *l);
int		api_ease_exp_in(lua_State *l);
int		api_ease_exp_out(lua_State *l);
int		api_ease_exp(lua_State *l);
int		api_ease_circ_in(lua_State *l);
int		api_ease_circ_out(lua_State *l);
int		api_ease_circ(lua_State *l);
int		api_ease_back_in(lua_State *l);
int		api_ease_back_out(lua_State *l);
int		api_ease_back(lua_State *l);
int		api_ease_elastic_in(lua_State *l);
int		api_ease_elastic_out(lua_State *l);
int		api_ease_elastic(lua_State *l);
int		api_ease_bounce_in(lua_State *l);
int		api_ease_bounce_out(lua_State *l);
int		api_ease_bounce(lua_State *l);

#endif

