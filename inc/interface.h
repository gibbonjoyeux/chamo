/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef INTERFACE_H
# define INTERFACE_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////////////////////////

typedef struct s_ui_cell			t_ui_cell;
typedef struct s_ui_button			t_ui_button;
typedef struct s_ui_toggle			t_ui_toggle;
typedef struct s_ui_checkbox		t_ui_checkbox;
typedef struct s_ui_slider			t_ui_slider;
typedef struct s_ui_zone_slider		t_ui_zone_slider;
typedef struct s_ui_textbox			t_ui_textbox;
typedef struct s_ui_list			t_ui_list;

////////////////////////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////////////////////////

struct			s_ui_cell {
	bool		value;		// Is cell selected
	bool		highlight;	// Is cell highlighted (set by user)
	bool		disabled;	// Can cell be selected or edited or not
};

struct			s_ui_button {
	bool		value;		// Is button clicked
};

struct			s_ui_toggle {
	bool		value;		// Is toggle button active (pressed)
};

struct			s_ui_checkbox {
	bool		value;		// Is checkbox checked
};

struct			s_ui_slider {
	double		value;		// Slider value (0 to 1)
	double		prev_value;
	int			width;
	bool		updated;
};

struct			s_ui_zone_slider {
	double		from;		// Zone slider "from" value (0 to 1 with from <= to)
	double		to;			// Zone slider "to" value (0 to 1 with to >= from)
	double		prev_from;
	double		prev_to;
	int			width;
	bool		updated;
};

struct			s_ui_textbox {
	char		*value;		// Textbox text content
	int			cursor;
	bool		focus;
	int			width;
	int			height;
};

struct			s_ui_list {
	int			value;		// List selected item
	bool		focus;
};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

// TODO
void		ui_init_button(t_ui_button *btn);
void		ui_init_checkbox(t_ui_checkbox *box);
void		ui_init_slider(t_ui_slider *slider);
void		ui_init_zone_slider(t_ui_zone_slider *slider);
void		ui_init_textbox(t_ui_textbox *box);

// RETURN true WHEN PRESSED OR UPDATED
void		ui_put_text(int x, int y, int w, int h, char *txt);
bool		ui_put_cell(int x, int y, t_ui_cell *btn);
bool		ui_put_button(int x, int y, t_ui_button *btn);
bool		ui_put_toggle(int x, int y, t_ui_toggle *btn);
bool		ui_put_checkbox(int x, int y, t_ui_checkbox *box);
bool		ui_put_slider(int x, int y, t_ui_slider *slider);
bool		ui_put_zone_slider(int x, int y, t_ui_zone_slider *slider);
bool		ui_put_textbox(int x, int y, t_ui_textbox *box);

#endif

