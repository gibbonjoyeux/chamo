# API

## MODES

- [x] `specs(samplerate, bitrate)`		Set samplerate & bitrate (default, 48000, 32bps)
- [x] `format("classic", width)`		coord range of [0, width]
- [x] `format("centered", width)`		coord range of [-width, width]
- [x] `format("sound")`					default, coord range of [-1, 1]

## BASICS

- [x] `save(path)`						Save audio to path (as .wav file) and exit
- [x] `save(path, exit)`				Save audio to path (as .wav file), exit if 2nd parameter is true
- [x] `exit()`
- [x] `time()`							Returns time in seconds depending of the mode
- [x] `time_bpm(bpm)`					Returns time accelerated with defined bpm
- [x] `samplecount()`					Returns current sample index
- [x] `samplerate()`					Returns samplerate


- [x] `cos(x)`							cos,acos,sin,asin,tan,atan,atan2 use a complete circle range of [0, 1]
- [x] `sin(x)`
- [x] `tan(x)`
- [x] `acos(x)`
- [x] `asin(x)`
- [x] `atan(x)`
- [x] `atan2(y, x)`
- [x] `max(a, b)`
- [x] `min(a, b)`
- [x] `constrain(x, min, max)`
- [x] `map(t, start1, stop1, start2, stop2, [constrain])`
- [x] `lerp(t, start, stop, [constrain])`
- [x] `floor(x)`
- [x] `ceil(x)`
- [x] `round(x)`
- [x] `rand()`								Returns random number [0, 1]
- [x] `rand(max)`							Returns random number [0, max]
- [x] `rand(min, max)`						Returns random number [min, max]
- [x] `noise(octaves, x)`					Returns 1D noise [-1, 1]
- [x] `noise(octaves, x, y)`				Returns 2D noise [-1, 1]
- [x] `noise(octaves, x, y, z)`				Returns 3D noise [-1, 1]
- [x] `noise(octaves, x, y, z, w)`			Returns 4D noise [-1, 1]


- [ ] `curve_point(x0, y0, x1, y1, x2, y2, x3, y3, t)`	Get point at t
- [ ] `curve_point(x0, y0, x1, y1, x2, y2, t)`			Get point at t
- [x] `freq(note)`							Returns frequency of note (ex: freq("A4") = 440, freq("c4") = 554.37). Upper cases for regular, lower cases for sharp.

## DRAWING

- [x] `point(x, y, [wait])`
- [x] `line(x1, y1, x2, y2, points)`
- [x] `circle(x, y, r, points)`
- [x] `polygon(x, y, r, vertices, points, [angle])`
- [x] `curve(x0, y0, x1, y1, x2, y2, points)`
- [x] `curve(x0, y0, x1, y1, x2, y2, x3, y3, points)`

### SHAPE

- [x] `shape.begin()`			Begin shape building
- [x] `shape.complete(points)`	End shape building and draws it with `points`
- [x] `shape.record()`			End shape building and returns its record
- [ ] `shape.record(len)`		End shape building and returns a table of `len` samples
- [ ] `shape.load(file)`		Load svg file and returns list of shapes
- [x] `shape.draw(my_shape, points)`	Draw recorded shape with `points`
- [x] `shape.get(my_shape, t)`			Get shape coord (x, y) at time `t` [0-1]
- [x] `shape.vertex(x, y)`						Put linear vertex on shape
- [x] `shape.vertex(x0, y0, x1, y1)`			Put quadratic bezier vertex on shape
- [x] `shape.vertex(x0, y0, x1, y1, x2, y2)`	Put cubic bezier vertex on shape

#### SHAPE OBJECT

- [x] `my_shape:draw(points)`	Draw shape with `points`
- [x] `my_shape:get(t)`			Get shape coord (x, y) at time `t` [0-1]

### SAMPLE

- [ ] `sample.load(path)`			Load wave file (.wav) and returns it
- [ ] `sample.draw(my_sample[, points])`	Draw sample

#### SAMPLE OBJECT

- [ ] `my_sample:draw([points])`	Draw sample with `points`
- [ ] `my_sample:get(t)`			Get sample coord (x, y) at time `t` [0-1]
- [ ] `my_sample.samplerate`		Sample samplerate
- [ ] `my_sample.samples`			Sample number of samples
- [ ] `my_sample.length`			Sample time in seconds

## MATRIX

- [x] `translate([x, y])`
- [x] `rotate([a])`
- [x] `scale([s])`
- [x] `push()`
- [x] `pop()`

## WAVES & EASING

By default, all wave functions take a number in the range [0-1] and return a number in the same range [0-1]. To map the value to an other range, you can use the `map()` or `lerp()` functions or provide two extra parameters to the wave function itself (ex: wave.sin(x, -25, 25) will return a value in the [-25, 25] range).

- [x] `wave.sin(t)`								Sine wave
- [x] `wave.tri(t)`								Triangle wave
- [x] `wave.squ(t)`								Square wave
- [x] `wave.saw(t)`								Sawtooth wave
- [x] `wave.rsaw(t)`							Reverse sawtooth wave
- [x] `wave.pul(t, duty)`						Pulse wave (duty cycle from 0 to 100)
- [x] `wave.asin(t)`							Absolute sine wave
- [x] `wave.hsin(t)`							Half sine wave
- [x] `wave.strsaw(t, steps)`					Sawtooth stairs wave
- [x] `wave.strtri(t, steps)`					Triangle stairs wave


- [x] `wave.linear(t)`
- [x] `wave.sine(t)`
- [x] `wave.sine_in(t)`
- [x] `wave.sine_out(t)`
- [x] `wave.quad(t)`
- [x] `wave.quad_in(t)`
- [x] `wave.quad_out(t)`
- [x] `wave.cubic(t)`
- [x] `wave.cubic_in(t)`
- [x] `wave.cubic_out(t)`
- [x] `wave.quart(t)`
- [x] `wave.quart_in(t)`
- [x] `wave.quart_out(t)`
- [x] `wave.quint(t)`
- [x] `wave.quint_in(t)`
- [x] `wave.quint_out(t)`
- [x] `wave.exp(t)`
- [x] `wave.exp_in(t)`
- [x] `wave.exp_out(t)`
- [x] `wave.circ(t)`
- [x] `wave.circ_in(t)`
- [x] `wave.circ_out(t)`
- [x] `wave.back(t)`
- [x] `wave.back_in(t)`
- [x] `wave.back_out(t)`
- [x] `wave.elastic(t)`
- [x] `wave.elastic_in(t)`
- [x] `wave.elastic_out(t)`
- [x] `wave.bounce(t)`
- [x] `wave.bounce_in(t)`
- [x] `wave.bounce_out(t)`

## EVENTS

- [x] `mouse.x`				Mouse x coord
- [x] `mouse.y`				Mouse y coord
- [x] `mouse.down`			True if mouse left click is down
- [ ] `mouse.press`			True if mouse left click just got pressed
- [ ] `key.down`			True if a keyboard key is down
- [ ] `key.press`			True if a keyboard key just got pressed
- [ ] `key.keys[]`			Contains keys value, true if actually down

### OSC

There are two ways to handle incoming OSC events.
Either via a single function `osc(path, ...)` or via a function tree.

- [x] `osc(path, ...)`		Single osc function
- [x] `osc.func(...)`		Path osc functions
- [x] `osc.table.func(...)`	Path osc functions
