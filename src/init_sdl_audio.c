
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			sdl_audio_callback(
	void			*userdata,
	u8				*stream,
	int				len) {

	len = len / (sizeof(float) * 2);
	/// COMPUTE SAMPLES
	pthread_mutex_lock(&(c.mutex));
		while (w.samples.count < w.samples.count_sent + len)
			compile_module_turn();
	pthread_mutex_unlock(&(c.mutex));
	/// SEND SAMPLES (AUDIO & VIDEO)
	live_send_samples((float*)stream, len);
	w.samples.count_sent += len;
	(void)userdata;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void				init_sdl_audio() {
	SDL_AudioSpec	spec;

	mem_clean(&spec, sizeof(SDL_AudioSpec));
	spec.freq = 48000;
	spec.format = AUDIO_F32SYS;
	spec.channels = 2;
	spec.samples = LIVE_SDL_SAMPLES;
	spec.userdata = NULL;
	spec.callback = sdl_audio_callback;
	c.live.audio_id = SDL_OpenAudioDevice(NULL, 0, &spec, &spec,
	SDL_AUDIO_ALLOW_FREQUENCY_CHANGE);
	SDL_PauseAudioDevice(c.live.audio_id, SDL_TRUE);
	if (c.live.audio_id == 0)
		exit_fail((char*)SDL_GetError());
}
