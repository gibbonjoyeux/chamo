
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		init_lua(char *file_path) {
	/// GET FILE PATH
	if (file_path == NULL)
		file_path = "main.lua";
	/// CREATE LUA VM
	l = luaL_newstate();
	/// INIT LIBRARIES
	luaL_openlibs(l);
	/// INIT FUNCTIONS
	init_global_tables();
	init_global_functions();
	/// LOAD USER MODULE
	if (luaL_dofile(l, file_path) != 0)
		exit_fail((char*)lua_tostring(l, 1));
}
