
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void				init_sdl() {
	/// INIT
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
		exit_fail("Cannot init SDL");
	/// CREATE WINDOW
	c.window = SDL_CreateWindow( "CHAMO", SDL_WINDOWPOS_UNDEFINED,
	SDL_WINDOWPOS_UNDEFINED, 800, 800, SDL_WINDOW_SHOWN);
	if (c.window == NULL)
		exit_fail("Cannot init SDL video");
	/// CREATE RENDERER
	c.renderer = SDL_CreateRenderer( c.window, -1, SDL_RENDERER_ACCELERATED );
	if (c.renderer == NULL)
		exit_fail("Cannot init SDL video");
}
