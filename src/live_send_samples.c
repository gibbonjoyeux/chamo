
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void				live_send_samples(float *buffer, size_t len) {
	t_list			*lst, *prev_lst;
	double			*content;
	double			x, y;
	size_t			i;

	/// CLEAN SCREEN
	SDL_SetRenderDrawBlendMode(c.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(c.renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(c.renderer, NULL);
	/// COMPUTE
	/// GET LAST CHUNK
	prev_lst = NULL;
	lst = w.samples.chunks.stack;
	while (lst->next != NULL) {
		prev_lst = lst;
		lst = lst->next;
	}
	content = (double*)&(lst->content);
	i = 0;
	while (i < len) {
		/// AUDIO
		buffer[i * 2] = MAP(content[c.live.play_index * 2],
		/**/ w.format.map_from.x, w.format.map_to.x, -1, 1);
		buffer[i * 2 + 1] = MAP(content[c.live.play_index * 2 + 1],
		/**/ w.format.map_from.y, w.format.map_to.y, 1, -1);
		/// VIDEO
		x = MAP(content[c.live.play_index * 2],
		/**/ w.format.map_from.x, w.format.map_to.x, 0, 800);
		y = MAP(content[c.live.play_index * 2 + 1],
		/**/ w.format.map_from.y, w.format.map_to.y, 0, 800);
		//// DRAW LINE SLIGHTLY
		SDL_SetRenderDrawColor(c.renderer, 255, 255, 0, 255 / 4);
		SDL_RenderDrawLine(c.renderer, c.live.last_point_x,
		c.live.last_point_y, x, y);
		//// DRAW POINT
		SDL_SetRenderDrawColor(c.renderer, 255, 255, 0, 255);
		SDL_RenderDrawPoint(c.renderer, x, y);
		/// UPDATE
		c.live.last_point_x = x;
		c.live.last_point_y = y;
		c.live.play_index += 1;
		i += 1;
		/// CHECK OVERFLOW
		if (c.live.play_index >= CHUNK_NUM_SAMPLES) {
			c.live.play_index = 0;
			lst_push(&(w.samples.chunks.pool), lst);
			/// CHUNK IS NOT LAST
			if (prev_lst != NULL) {
				prev_lst->next = NULL;
				/// GET LAST CHUNK
				prev_lst = NULL;
				lst = w.samples.chunks.stack;
				while (lst->next != NULL) {
					prev_lst = lst;
					lst = lst->next;
				}
				content = (double*)&(lst->content);
			/// CHUNK IS LAST (NO MORE SOUND TO DRAW)
			} else {
				w.samples.chunks.stack = NULL;
				break;
			}
		}
	}
	/// RENDER
	SDL_RenderPresent(c.renderer);
}

