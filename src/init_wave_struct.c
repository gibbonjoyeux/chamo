
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void		init_wave_struct(void) {
	time_t	t;

	srand((unsigned)time(&t));
	/// CHUNKS
	w.samples.count = 0;
	w.samples.count_sent = 0;
	w.samples.chunk_index = 0;
	pool_init(&(w.samples.chunks), 0);
	VEC_SET(w.samples.last, 0, 0);
	/// SHAPE
	pool_init(&(w.shape.vertices), sizeof(t_vertex));
	w.shape.last_point = NULL;
	w.shape.precision = SHAPE_DEFAULT_PRECISION;
	/// MATRIX
	pool_init(&(w.matrices), sizeof(t_matrix));
	/// FORMAT
	init_format(DEFAULT_FORMAT_MODE, DEFAULT_FORMAT_WIDTH);
	/// ENV
	reset_env();
	/// SOUND SPECS
	wave_set_specs(DEFAULT_SAMPLERATE, DEFAULT_BITSPERSAMPLE);
}
