
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void		close_wave_struct(void) {
	/// PUT SAMPLES CHUNKS TO CHUNK POOL
	if (w.samples.chunks.stack != NULL)
		pool_reset(&(w.samples.chunks));
	/// PUT MATRIX STACK TO MATRIX POOL
	if (w.matrices.stack != NULL)
		pool_reset(&(w.matrices));
}
