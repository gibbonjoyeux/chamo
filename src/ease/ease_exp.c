/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

double		ease_exp_in(double x) {
	return x == 0 ? 0 : pow(2, 10 * x - 10);
}

double		ease_exp_out(double x) {
	return x == 1 ? 1 : 1 - pow(2, -10 * x);
}

double		ease_exp(double x) {
	return x == 0
	? 0
	: x == 1
	? 1
	: x < 0.5 ? pow(2, 20 * x - 10) / 2
	: (2 - pow(2, -20 * x + 10)) / 2;
}
