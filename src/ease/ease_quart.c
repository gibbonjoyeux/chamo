/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

double		ease_quart_in(double x) {
	return x * x * x * x;
}

double		ease_quart_out(double x) {
	return 1 - pow(1 - x, 4);
}

double		ease_quart(double x) {
	return x < 0.5 ? 8 * x * x * x * x : 1 - pow(-2 * x + 2, 4) / 2;
}
