/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

double		ease_elastic_in(double x) {
	double	c4;

	c4 = (2 * PI) / 3;
	return x == 0
	? 0
	: x == 1
	? 1
	: -pow(2, 10 * x - 10) * sin((x * 10 - 10.75) * c4);
}

double		ease_elastic_out(double x) {
	double	c4;

	c4 = (2 * PI) / 3;
	return x == 0
	? 0
	: x == 1
	? 1
	: pow(2, -10 * x) * sin((x * 10 - 0.75) * c4) + 1;
}

double		ease_elastic(double x) {
	double	c5;

	c5 = (2 * PI) / 4.5;
	return x == 0
	? 0
	: x == 1
	? 1
	: x < 0.5
	? -(pow(2, 20 * x - 10) * sin((20 * x - 11.125) * c5)) / 2
	: (pow(2, -20 * x + 10) * sin((20 * x - 11.125) * c5)) / 2 + 1;
}
