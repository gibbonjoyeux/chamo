/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static double	ease_out_bounce(double x) {
	if (x < 4/11.0)
		return (121 * x * x)/16.0;
	else if (x < 8/11.0)
		return (363/40.0 * x * x) - (99/10.0 * x) + 17/5.0;
	else if (x < 9/10.0)
		return (4356/361.0 * x * x) - (35442/1805.0 * x) + 16061/1805.0;
	else
		return (54/5.0 * x * x) - (513/25.0 * x) + 268/25.0;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

double		ease_bounce_in(double x) {
	return 1 - ease_out_bounce(1 - x);
}

double		ease_bounce_out(double x) {
	return ease_out_bounce(x);
}

double		ease_bounce(double x) {
	return x < 0.5
	? (1 - ease_out_bounce(1 - 2 * x)) / 2
	: (1 + ease_out_bounce(2 * x - 1)) / 2;
}
