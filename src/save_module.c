
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	call_user_init(int argc, char **argv) {
	int		len;
	int		i;

	// GET FUNCTION
	lua_getglobal(l, "init");
	// PUSH PARAMETERS
	len = MAX(0, argc - 2);
	for (i = 0; i < len; ++i)
		lua_pushstring(l, argv[2 + i]);
	// CALL FUNCTION
	lua_call(l, len, 0);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		save_module(int argc, char **argv) {
	/// MODE
	c.mode = MODE_SAVE;
	/// INIT
	init_lua(argv[1]);
	init_chamo_struct();
	init_wave_struct();
	call_user_init(argc, argv);
	wave_new_chunk();
	/// RUN
	while (true)
		compile_module_turn();
}
