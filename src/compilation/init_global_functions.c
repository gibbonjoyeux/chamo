/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	put_to_table(char *name, int (*f)(lua_State*)) {
	lua_pushcfunction(l, f);
	lua_setfield(l, -2, name);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		init_global_functions(void) {
	/// MODE & FORMAT
	lua_register(l, "specs", api_specs);
	lua_register(l, "format", api_format);

	/// BASICS
	lua_register(l, "time", api_time);
	lua_register(l, "time_bpm", api_time_bpm);
	lua_register(l, "samplecount", api_samplecount);
	lua_register(l, "samplerate", api_samplerate);

	lua_register(l, "save", api_save);
	lua_register(l, "exit", api_exit);

	/// MATH
	lua_register(l, "cos", api_cos);
	lua_register(l, "sin", api_sin);
	lua_register(l, "tan", api_tan);
	lua_register(l, "acos", api_acos);
	lua_register(l, "asin", api_asin);
	lua_register(l, "atan", api_atan);
	lua_register(l, "atan2", api_atan2);
	lua_register(l, "min", api_min);
	lua_register(l, "max", api_max);
	lua_register(l, "constrain", api_constrain);
	lua_register(l, "map", api_map);
	lua_register(l, "lerp", api_lerp);
	lua_register(l, "floor", api_floor);
	lua_register(l, "ceil", api_ceil);
	lua_register(l, "round", api_round);
	lua_register(l, "rand", api_rand);
	lua_register(l, "noise", api_noise);

	/// DRAW
	lua_register(l, "wait", api_draw_wait);
	lua_register(l, "point", api_draw_point);
	lua_register(l, "line", api_draw_line);
	lua_register(l, "circle", api_draw_circle);
	lua_register(l, "polygon", api_draw_polygon);
	lua_register(l, "curve", api_draw_curve);

	lua_register(l, "translate", api_matrix_translate);
	lua_register(l, "scale", api_matrix_scale);
	lua_register(l, "rotate", api_matrix_rotate);
	lua_register(l, "push", api_matrix_push);
	lua_register(l, "pop", api_matrix_pop);

	lua_register(l, "limit", api_limit);
	lua_register(l, "last_point", api_lastpoint);

	lua_register(l, "point_noise", api_pointnoise);
	lua_register(l, "point_wait", api_pointwait);

	lua_register(l, "freq", api_freq);

	//// SHAPE TABLE
	lua_newtable(l);
	luaL_setmetatable(l, "_chamo_meta_shape");
	lua_setglobal(l, "shape");

	/// WAVE TABLE
	lua_createtable(l, 0, 40);
		put_to_table("sin", api_wave_sin);
		put_to_table("cos", api_wave_cos);
		put_to_table("tri", api_wave_tri);
		put_to_table("squ", api_wave_squ);
		put_to_table("saw", api_wave_saw);
		put_to_table("rsaw", api_wave_rsaw);
		put_to_table("pul", api_wave_pul);
		put_to_table("asin", api_wave_asin);
		put_to_table("hsin", api_wave_hsin);
		put_to_table("strsaw", api_wave_strsaw);
		put_to_table("strtri", api_wave_strtri);
		put_to_table("linear", api_ease_linear);
		put_to_table("sine_in", api_ease_sine_in);
		put_to_table("sine_out", api_ease_sine_out);
		put_to_table("sine", api_ease_sine);
		put_to_table("quad_in", api_ease_quad_in);
		put_to_table("quad_out", api_ease_quad_out);
		put_to_table("quad", api_ease_quad);
		put_to_table("cubic_in", api_ease_cubic_in);
		put_to_table("cubic_out", api_ease_cubic_out);
		put_to_table("cubic", api_ease_cubic);
		put_to_table("quart_in", api_ease_quart_in);
		put_to_table("quart_out", api_ease_quart_out);
		put_to_table("quart", api_ease_quart);
		put_to_table("quint_in", api_ease_quint_in);
		put_to_table("quint_out", api_ease_quint_out);
		put_to_table("quint", api_ease_quint);
		put_to_table("exp_in", api_ease_exp_in);
		put_to_table("exp_out", api_ease_exp_out);
		put_to_table("exp", api_ease_exp);
		put_to_table("circ_in", api_ease_circ_in);
		put_to_table("circ_out", api_ease_circ_out);
		put_to_table("circ", api_ease_circ);
		put_to_table("back_in", api_ease_back_in);
		put_to_table("back_out", api_ease_back_out);
		put_to_table("back", api_ease_back);
		put_to_table("elastic_in", api_ease_elastic_in);
		put_to_table("elastic_out", api_ease_elastic_out);
		put_to_table("elastic", api_ease_elastic);
		put_to_table("bounce_in", api_ease_bounce_in);
		put_to_table("bounce_out", api_ease_bounce_out);
		put_to_table("bounce", api_ease_bounce);
	lua_setglobal(l, "wave");
}
