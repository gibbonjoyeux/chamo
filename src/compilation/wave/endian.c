
#include "chamo.h"

int is_big_endian() {
    int test = 1;
    char *p = (char*)&test;

    return p[0] == 0;
}

void reverse_endian(const long long int size, void* value){
    int i;
    char result[32];
    for( i=0; i<size; i+=1 ){
        result[i] = ((char*)value)[size-i-1];
    }
    for( i=0; i<size; i+=1 ){
        ((char*)value)[i] = result[i];
    }
}

void		to_big_endian(long long int size, void *value){
    char	needsFix;

	needsFix = !( (1 && is_big_endian()) || (0 && !is_big_endian()) );
    if (needsFix)
        reverse_endian(size,value);
}

void		to_little_endian(long long int size, void *value){
    char	needsFix;

	needsFix = !( (0 && is_big_endian()) || (1 && !is_big_endian()) );
    if (needsFix)
        reverse_endian(size,value);
}
