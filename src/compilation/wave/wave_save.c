//
// 8bits	-	unsigned
// 16bits	-	signed
// 24bits	-	signed
// 32bits	-	signed
//

#include "chamo.h"

#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		add_samples_8bit(void *buffer, double *content, size_t len) {
	uint8_t		sample_8bit;
	size_t		i;

	for (i = 0; i < len; ++i) {
		/// GET VALUE X
		if (i % 2 == 0) {
			sample_8bit = MAP(content[i],
			/**/ w.format.map_from.x, w.format.map_to.x,
			/**/ 0, 255);
		/// GET VALUE Y
		} else {
			sample_8bit = MAP(content[i],
			/**/ w.format.map_from.y, w.format.map_to.y,
			/**/ 0, 255);
		}
		to_little_endian(1, (void*)&sample_8bit);
		/// PUT VALUE
		((uint8_t*)buffer)[i] = sample_8bit;
	}
}

static void		add_samples_16bit(void *buffer, double *content, size_t len) {
	int16_t		sample_16bit;
	size_t		i;

	for (i = 0; i < len; ++i) {
		/// GET VALUE X
		if (i % 2 == 0) {
			sample_16bit = MAP(content[i],
			/**/ w.format.map_from.x, w.format.map_to.x,
			/**/ -32768, 32767);
		/// GET VALUE Y
		} else {
			sample_16bit = MAP(content[i],
			/**/ w.format.map_from.y, w.format.map_to.y,
			/**/ -32768, 32767);
		}
		to_little_endian(2, (void*)&sample_16bit);
		/// PUT VALUE
		((int16_t*)buffer)[i] = sample_16bit;
	}
}

static void		add_samples_24bit(void *buffer, double *content, size_t len) {
	struct {
		int32_t	value : 24;
	}			sample_24bit;
	size_t		i;

	for (i = 0; i < len; ++i) {
		/// GET VALUE X
		if (i % 2 == 0) {
			sample_24bit.value = MAP(content[i],
			/**/ w.format.map_from.x, w.format.map_to.x,
			/**/ -8388608, 8388607);
		/// GET VALUE Y
		} else {
			sample_24bit.value = MAP(content[i],
			/**/ w.format.map_from.y, w.format.map_to.y,
			/**/ -8388608, 8388607);
		}
		to_little_endian(3, (void*)&sample_24bit);
		/// PUT VALUE
		((int8_t*)buffer)[i * 3] = ((int8_t*)&sample_24bit)[0];
		((int8_t*)buffer)[i * 3 + 1] = ((int8_t*)&sample_24bit)[1];
		((int8_t*)buffer)[i * 3 + 2] = ((int8_t*)&sample_24bit)[2];
	}
}

static void		add_samples_32bit(void *buffer, double *content, size_t len) {
	int32_t		sample_32bit;
	size_t		i;

	for (i = 0; i < len; ++i) {
		/// GET VALUE X
		if (i % 2 == 0) {
			sample_32bit = MAP(content[i],
			/**/ w.format.map_from.x, w.format.map_to.x,
			/**/ -2147483648, 2147483647);
		/// GET VALUE Y
		} else {
			sample_32bit = MAP(content[i],
			/**/ w.format.map_from.y, w.format.map_to.y,
			/**/ -2147483648, 2147483647);
		}
		to_little_endian(4, (void*)&sample_32bit);
		/// PUT VALUE
		((int32_t*)buffer)[i] = sample_32bit;
	}
}

static void		write_header(int fd) {
	// CHECK ENDIANESS
	to_little_endian(sizeof(int), (void*)&(w.header.chunkSize));
	to_little_endian(sizeof(int), (void*)&(w.header.subChunk1Size));
	to_little_endian(sizeof(short int), (void*)&(w.header.audioFormat));
	to_little_endian(sizeof(short int), (void*)&(w.header.numChannels));
	to_little_endian(sizeof(int), (void*)&(w.header.sampleRate));
	to_little_endian(sizeof(int), (void*)&(w.header.byteRate));
	to_little_endian(sizeof(short int), (void*)&(w.header.blockAlign));
	to_little_endian(sizeof(short int), (void*)&(w.header.bitsPerSample));
	to_little_endian(sizeof(int), (void*)&(w.header.subChunk2Size));
	// WRITE
	write(fd, &(w.header), sizeof(t_wave_header));
}

static void		write_content(int fd, void *buffer) {
	void		(*translate_func)(void*, double*, size_t);
	t_list		*lst, *elem;
	size_t		len, size;
	double		*content;

	/// GET SAMPLE FUNCTION
	if (w.header.bitsPerSample == 8)
		translate_func = add_samples_8bit;
	else if (w.header.bitsPerSample == 16)
		translate_func = add_samples_16bit;
	else if (w.header.bitsPerSample == 24)
		translate_func = add_samples_24bit;
	else if (w.header.bitsPerSample == 32)
		translate_func = add_samples_32bit;
	else
		exit_fail("Unknown bits per sample value");
	/// INIT CHUNKS
	lst_rev(&(w.samples.chunks.stack));
	lst = w.samples.chunks.stack;
	/// FOR EACH CHUNK
	while (lst != NULL) {
		/// GET CHUNK CONTENT & SIZE
		if (lst->next != NULL)
			len = 2 * CHUNK_NUM_SAMPLES;
		else
			len = 2 * w.samples.chunk_index;
		size = len * (w.header.bitsPerSample / 8);
		elem = lst_pop(&lst);
		content = (double*)&(elem->content);
		/// TRANSLATE & WRITE CONTENT
		translate_func(buffer, content, len);
		write(fd, buffer, size);
		lst_push(&(w.samples.chunks.pool), elem);
	}
	w.samples.chunks.stack = NULL;
	w.samples.count = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void			wave_save(char *path) {
	void		*buffer;
	size_t		num_chunks;
	size_t		total_size;
	int			fd;

	/// GET DATA BUFFER
	buffer = malloc(CHUNK_LEN * w.header.byteRate);
	if (buffer == NULL)
		exit_fail("Not enough memory to save");
	/// GET TOTAL SIZE
	num_chunks = lst_len(w.samples.chunks.stack);
	total_size = (num_chunks - 1) * (CHUNK_LEN * w.header.byteRate);
	total_size += w.samples.chunk_index * w.header.numChannels
	* (w.header.bitsPerSample / 8);
	/// UPDATE HEADER
	w.header.chunkSize = 4 + 8 + 16 + 8 + total_size;
	w.header.subChunk2Size = total_size;
	/// SAVE TO FILE
	fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0777);
	write_header(fd);
	write_content(fd, buffer);
	close(fd);
	free(buffer);
}
