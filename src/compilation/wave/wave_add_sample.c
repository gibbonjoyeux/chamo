
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			put_sample(t_vec p) {
	double			*buffer;

	/// CHECK CHUNK OVERFLOW
	if (w.samples.chunks.stack == NULL
	|| w.samples.chunk_index >= CHUNK_NUM_SAMPLES)
		wave_new_chunk();
	/// PUT SAMPLE
	buffer = (double*)&(w.samples.chunks.stack->content);
	buffer[w.samples.chunk_index * 2] = p.x;
	buffer[w.samples.chunk_index * 2 + 1] = p.y;
	/// UPDATE VARIABLES
	w.samples.count += 1;
	w.samples.chunk_index += 1;
}

static inline bool	put_sample_point(t_vec p) {
	/// CHECK SAMPLE OVERFLOW & LIMIT OVERFLOW
	if (w.samples.limit_set == true && w.samples.limit_count <= 0)
		return false;
	/// PUT SAMPLE
	put_sample(p);
	/// UPDATE VARIABLES
	w.samples.limit_count -= 1;
	w.samples.last = p;
	return true;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool			wave_add_sample(t_vec p) {
	int			i;

	/// HANDLE POINT NOISE
	if (w.point.noise > 0) {
		p.x += ((double)rand() / RAND_MAX) * w.point.noise - w.point.noise / 2;
		p.y += ((double)rand() / RAND_MAX) * w.point.noise - w.point.noise / 2;
	}
	/// FOR EACH POINT WAIT
	for (i = 0; i < w.point.wait; ++i)
		if (put_sample_point(p) == false)
			return false;
	return true;
}
