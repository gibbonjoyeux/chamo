
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void		init_header(int sample_rate, short int bits_per_sample) {
	// RIFF WAVE Header
    w.header.chunkId[0] = 'R';
    w.header.chunkId[1] = 'I';
    w.header.chunkId[2] = 'F';
    w.header.chunkId[3] = 'F';
    w.header.format[0] = 'W';
    w.header.format[1] = 'A';
    w.header.format[2] = 'V';
    w.header.format[3] = 'E';

    // Format subchunk
    w.header.subChunk1Id[0] = 'f';
    w.header.subChunk1Id[1] = 'm';
    w.header.subChunk1Id[2] = 't';
    w.header.subChunk1Id[3] = ' ';
    w.header.audioFormat = 1; // FOR PCM
    w.header.numChannels = 2; // 1 for MONO, 2 for stereo
    w.header.sampleRate = sample_rate; // ie 44100 hertz, cd quality audio
    w.header.bitsPerSample = bits_per_sample; // 8 / 16 / 32 bits
    w.header.byteRate = w.header.sampleRate * w.header.numChannels * w.header.bitsPerSample / 8;
    w.header.blockAlign = w.header.numChannels * w.header.bitsPerSample / 8;

    // Data subchunk
    w.header.subChunk2Id[0] = 'd';
    w.header.subChunk2Id[1] = 'a';
    w.header.subChunk2Id[2] = 't';
    w.header.subChunk2Id[3] = 'a';

    // All sizes for later:
    // chuckSize = 4 + (8 + subChunk1Size) + (8 + subChubk2Size)
    // subChunk1Size is constanst, i'm using 16 and staying with PCM
    // subChunk2Size = nSamples * nChannels * bits_per_sample/8
    // Whenever a sample is added:
    //    chunkSize += (nChannels * bits_per_sample/8)
    //    subChunk2Size += (nChannels * bits_per_sample/8)
    w.header.chunkSize = 4 + 8 + 16 + 8 + 0;
    w.header.subChunk1Size = 16;
    w.header.subChunk2Size = 0;
}

static void	init_chunk_pool(void) {
	/// WAVE CHUNKS
	pool_free(&(w.samples.chunks));
	pool_init(&(w.samples.chunks), CHUNK_SIZE);
	w.samples.chunk_index = 0;
	/// LIVE CHUNKS
	c.live.chunk = NULL;
	c.live.play_index = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTIOS
////////////////////////////////////////////////////////////////////////////////

void		wave_set_specs(int sample_rate, short int bits_per_sample) {
	init_header(sample_rate, bits_per_sample);
	init_chunk_pool();
}
