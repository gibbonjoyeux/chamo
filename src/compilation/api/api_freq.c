/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void	compute_octave_simple(int note, int octave) {
	double	list[] = {27.500, 30.867, 16.351, 18.354, 20.601, 21.826, 24.499};
	double	freq;

	freq = list[note];
	while (octave > 0) {
		freq *= 2;
		octave -= 1;
	}
	lua_pushnumber(l, freq);
}

static void	compute_octave_sharp(int note, int octave) {
	double	list[] = {29.135, 32.703, 17.323, 19.445, 21.826, 23.124, 25.956};
	double	freq;

	freq = list[note];
	while (octave > 0) {
		freq *= 2;
		octave -= 1;
	}
	lua_pushnumber(l, freq);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			api_freq(lua_State *l) {
	char	*note;

	note = (char*)lua_tostring(l, 1);
	if (note == NULL) {
		lua_pushnumber(l, 0);
		return 1;
	}
	if (note[0] >= 'A' && note[0] <= 'G')
		compute_octave_simple(note[0] - 'A', atoi(note + 1));
	else if (note[0] >= 'a' && note[0] <= 'g')
		compute_octave_sharp(note[0] - 'a', atoi(note + 1));
	else
		lua_pushnumber(l, 0);
	return 1;
}
