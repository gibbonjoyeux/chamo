/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

//////////////////////////////////////////////////////////////////////////////////
///// PRIVATE FUNCTIONS
//////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int				api_noise(lua_State *l) {
	double		x, y, z, w;
	double		factor;
	double		value, new_value;
	int			num_params;
	lint		octaves;
	lint		i;

	/// GET PARAMS
	num_params = lua_gettop(l);
	if (num_params < 2 || num_params > 5) {
		lua_pushnumber(l, 0);
		return 1;
	}
	octaves = get_param_int(1);
	x = get_param_float(2);
	y = (num_params > 2) ? get_param_float(3) : 0;
	z = (num_params > 3) ? get_param_float(4) : 0;
	w = (num_params > 4) ? get_param_float(5) : 0;
	/// COMPUTE NOISE
	value = 0;
	factor = 1;
	for (i = 0; i < octaves; ++i) {
		if (num_params == 2) {
			new_value = open_simplex_noise2(x * factor, 0);
		} else if (num_params == 3) {
			new_value = open_simplex_noise2(x * factor, y * factor);
		} else if (num_params == 4) {
			new_value = open_simplex_noise3(x * factor, y * factor, z * factor);
		} else {
			new_value = open_simplex_noise4(x * factor, y * factor, z * factor,
			/**/ w * factor);
		}
		value += new_value / factor;
		factor *= 2;
	}
	/// RETURN VALUE
	lua_pushnumber(l, value);
	return 1;
}
