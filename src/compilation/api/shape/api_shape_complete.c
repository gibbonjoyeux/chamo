/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			draw_shape_line(t_vec *last_point, t_vertex *vertex,
					int points) {
	draw_line(*last_point, vertex->base.coord, points);
}

static void			draw_shape_quad(t_vec *last_point, t_vertex *vertex,
					int points) {
	draw_bezier_quad(*last_point, vertex->extra.quad.controller_1,
	/**/ vertex->base.coord, points);
}

static void			draw_shape_cubic(t_vec *last_point, t_vertex *vertex,
					int points) {
	draw_bezier_cubic(*last_point, vertex->extra.cubic.controller_1,
	/**/ vertex->extra.cubic.controller_2, vertex->base.coord, points);
}

static void			draw_shape_arc(t_vec *last_point, t_vertex *vertex,
					int points) {
	//draw_arc(*last_point, vertex->center, vertex->radius, vertex->angle,
	///**/ vertex->direction, points)
	last_point = NULL;
	vertex = NULL;
	points = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

static void			(*vertex_functions[4])(t_vec*, t_vertex*, int) = {
	draw_shape_line,
	draw_shape_quad,
	draw_shape_cubic,
	draw_shape_arc
};

int					api_shape_complete(lua_State *l) {
	t_vertex		*vertex;
	t_vec			*last_point;
	t_list			*lst;
	lint			total_points, remaining_points, num_points;
	size_t			i;

	/// GET PARAM
	total_points = get_param_int(1);
	remaining_points = total_points;
	/// LOOP THROUGH VERTICES
	last_point = NULL;
	lst = w.shape.vertices.stack;
	for (i = 0; i < w.shape.num_vertices; ++i) {
		/// GET VERTEX
		vertex = (t_vertex*)&(lst->content);
		vertex->base.length /= w.shape.length;
		/// DRAW VERTEX
		if (i > 0) {
			if (i == w.shape.num_vertices - 1)
				num_points = remaining_points;
			else
				num_points = vertex->base.length * total_points;
			remaining_points -= num_points;
			vertex_functions[vertex->base.type](last_point, vertex, num_points);
		}
		last_point = &(vertex->base.coord);
		/// OFFSET LIST
		lst = lst->next;
	}
	pool_reset(&(w.shape.vertices));
	(void)l;
	return 0;
}
