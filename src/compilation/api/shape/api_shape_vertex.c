/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static double	dist(t_vec vec1, t_vec vec2) {
	t_vec			diff;

	diff.x = vec1.x - vec2.x;
	diff.y = vec1.y - vec2.y;
	return sqrt(diff.x * diff.x + diff.y * diff.y);
}

static size_t	compute_linear_default(t_vertex *vertex, int num_params) {
	/// INIT
	vertex->base.type = VERTEX_LINEAR;
	vertex->base.coord = get_param_coord(num_params - 1);
	/// COMPUTE LENGTH
	if (w.shape.last_point == NULL)
		vertex->base.length = 0;
	else
		vertex->base.length = dist(*w.shape.last_point, vertex->base.coord);
	return sizeof(t_vertex_base);
}

static size_t	compute_linear(t_vertex *vertex) {
	/// INIT
	vertex->base.type = VERTEX_LINEAR;
	vertex->base.coord = get_param_coord(1);
	/// COMPUTE LENGTH
	if (w.shape.last_point == NULL)
		vertex->base.length = 0;
	else
		vertex->base.length = dist(*w.shape.last_point, vertex->base.coord);
	return sizeof(t_vertex_base);
}

static size_t	compute_quadratic(t_vertex *vertex) {
	if (w.shape.first_point == NULL)
		return compute_linear_default(vertex, 4);
	/// INIT
	vertex->base.type = VERTEX_QUADRATIC;
	vertex->extra.quad.controller_1 = get_param_coord(1);
	vertex->base.coord = get_param_coord(3);
	/// COMPUTE LENGTH
	if (w.shape.last_point == NULL)
		vertex->base.length = 0;
	else
		vertex->base.length = dist(*w.shape.last_point, vertex->base.coord);
	return sizeof(t_vertex_base) + sizeof(t_vertex_quad);
}

static size_t	compute_cubic(t_vertex *vertex) {
	if (w.shape.first_point == NULL)
		return compute_linear_default(vertex, 6);
	/// INIT
	vertex->base.type = VERTEX_CUBIC;
	vertex->extra.cubic.controller_1 = get_param_coord(1);
	vertex->extra.cubic.controller_2 = get_param_coord(3);
	vertex->base.coord = get_param_coord(5);
	/// COMPUTE LENGTH
	if (w.shape.last_point == NULL)
		vertex->base.length = 0;
	else
		vertex->base.length = dist(*w.shape.last_point, vertex->base.coord);
	return sizeof(t_vertex_base) + sizeof(t_vertex_cubic);
}

static size_t	compute_arc(t_vertex *vertex) {
	t_vec		radius;
	t_vec		coord;
	double		angle;
	bool		flag_dir, flag_big;

	if (w.shape.first_point == NULL)
		return compute_linear_default(vertex, 7);
	/// GET PARAMS
	radius = get_param_coord(1);
	angle = get_param_float(3);
	flag_big = lua_toboolean(l, 4);
	flag_dir = lua_toboolean(l, 5);
	coord = get_param_coord(6);
	/// INIT
	vertex->base.type = VERTEX_ARC;
	vertex->base.coord = coord;
	vertex->extra.arc.radius = radius;
	vertex->extra.arc.angle = angle;
	vertex->extra.arc.direction = flag_dir;
	/// COMPUTE ARC CENTER
	
	/// COMPUTE LENGTH
	if (w.shape.last_point == NULL)
		vertex->base.length = 0;
	else
		vertex->base.length = dist(*w.shape.last_point, vertex->base.coord);
	return sizeof(t_vertex_base) + sizeof(t_vertex_arc);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int				api_shape_vertex(lua_State *l) {
	t_list		*new_item;
	t_vertex	*vertex;
	int			num_params;

	/// CHECK PARAMS
	num_params = lua_gettop(l);
	if (num_params != 2 && num_params != 4 && num_params != 6
	&& num_params != 7)
		return 0;
	/// CREATE VERTEX
	new_item = pool_append_item(&(w.shape.vertices));
	if (new_item == NULL)
		return 0;
	vertex = (t_vertex*)&(new_item->content);
	/// COMPUTE VERTEX
	if (num_params == 2)
		w.shape.alloc_size += compute_linear(vertex);
	else if (num_params == 4)
		w.shape.alloc_size += compute_quadratic(vertex);
	else if (num_params == 6)
		w.shape.alloc_size += compute_cubic(vertex);
	else if (num_params == 7)
		w.shape.alloc_size += compute_arc(vertex);
	/// UPDATE SHAPE
	w.shape.length += vertex->base.length;
	w.shape.num_vertices += 1;
	w.shape.last_point = &(vertex->base.coord);
	if (w.shape.first_point == NULL)
		w.shape.first_point = &(vertex->base.coord);
	l = NULL;
	return 0;
}
