/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int					api_shape_record(lua_State *l) {
	t_shape_user	*shape;
	int8_t			*buffer;
	t_vertex		*vertex;
	t_list			*lst;	

	/// ALLOC SHAPE
	shape = lua_newuserdata(l, sizeof(t_shape_user) + w.shape.alloc_size);
	if (shape == NULL)
		return 0;
	/// FILL SHAPE
	shape->num_vertices = w.shape.num_vertices;
	buffer = (int8_t*)&(shape->vertices);
	lst = w.shape.vertices.stack;
	while (lst != NULL) {
		/// GET VERTEX
		vertex = (t_vertex*)&(lst->content);
		/// NORMALIZE VERTEX LENGTH
		vertex->base.length /= w.shape.length;
		/// ADD VERTEX TO BUFFER
		*((t_vertex_base*)buffer) = vertex->base;
		buffer += sizeof(t_vertex_base);
		if (vertex->base.type == VERTEX_QUADRATIC) {
			*((t_vertex_quad*)buffer) = vertex->extra.quad;
			buffer += sizeof(t_vertex_quad);
		} else if (vertex->base.type == VERTEX_CUBIC) {
			*((t_vertex_cubic*)buffer) = vertex->extra.cubic;
			buffer += sizeof(t_vertex_cubic);
		} else if (vertex->base.type == VERTEX_ARC) {
			*((t_vertex_arc*)buffer) = vertex->extra.arc;
			buffer += sizeof(t_vertex_arc);
		}
		lst = lst->next;
	}
	pool_reset(&(w.shape.vertices));
	luaL_setmetatable(l, "_chamo_meta_shape");
	return 1;
}
