/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			draw_shape_line(t_vec *last_point, t_vertex *vertex,
					int points) {
	draw_line(*last_point, vertex->base.coord, points);
}

static void			draw_shape_quad(t_vec *last_point, t_vertex *vertex,
					int points) {
	draw_bezier_quad(*last_point, vertex->extra.quad.controller_1,
	/**/ vertex->base.coord, points);
}

static void			draw_shape_cubic(t_vec *last_point, t_vertex *vertex,
					int points) {
	draw_bezier_cubic(*last_point, vertex->extra.cubic.controller_1,
	/**/ vertex->extra.cubic.controller_2, vertex->base.coord, points);
}

static void			draw_shape_arc(t_vec *last_point, t_vertex *vertex,
					int points) {
	//draw_arc(*last_point, vertex->center, vertex->radius, vertex->angle,
	///**/ vertex->direction, points)
	last_point = NULL;
	vertex = NULL;
	points = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

static void			(*vertex_functions[4])(t_vec*, t_vertex*, int) = {
	draw_shape_line,
	draw_shape_quad,
	draw_shape_cubic,
	draw_shape_arc
};

static size_t		vertex_sizes[4] = {
	sizeof(t_vertex_base),
	sizeof(t_vertex_base) + sizeof(t_vertex_quad),
	sizeof(t_vertex_base) + sizeof(t_vertex_cubic),
	sizeof(t_vertex_base) + sizeof(t_vertex_arc)
};

int					api_shape_draw(lua_State *l) {
	t_shape_user	*shape;
	t_vertex		*vertex;
	t_vec			*last_point;
	int8_t			*buffer;
	lint			total_points, remaining_points, num_points;
	size_t			i;

	/// GET PARAMS
	shape = lua_touserdata(l, 1);
	total_points = get_param_int(2);
	remaining_points = total_points;
	/// LOOP THROUGH VERTICES
	last_point = NULL;
	buffer = (int8_t*)&(shape->vertices);
	for (i = 0; i < shape->num_vertices; ++i) {
		/// GET VERTEX
		vertex = (t_vertex*)buffer;
		/// DRAW VERTEX
		if (i > 0) {
			if (i == shape->num_vertices - 1)
				num_points = remaining_points;
			else
				num_points = vertex->base.length * total_points;
			remaining_points -= num_points;
			vertex_functions[vertex->base.type](last_point, vertex, num_points);
		}
		last_point = &(vertex->base.coord);
		/// OFFSET BUFFER
		buffer += vertex_sizes[vertex->base.type];
	}
	return 0;
}
