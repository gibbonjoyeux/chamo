/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void			draw_shape_line(t_vec *last_point, t_vertex *vertex,
					double t) {
	t_vec			value;

	VEC_BEZIER_LINEAR(*last_point, vertex->base.coord, t, value);
	lua_pushnumber(l, value.x);
	lua_pushnumber(l, value.y);
}

static void			draw_shape_quad(t_vec *last_point, t_vertex *vertex,
					double t) {
	t_vec			value;

	VEC_BEZIER_QUAD(*last_point, vertex->extra.quad.controller_1,
	/**/ vertex->base.coord, t, value);
	lua_pushnumber(l, value.x);
	lua_pushnumber(l, value.y);
}

static void			draw_shape_cubic(t_vec *last_point, t_vertex *vertex,
					double t) {
	t_vec			value;

	VEC_BEZIER_CUBIC(*last_point, vertex->extra.cubic.controller_1,
	/**/ vertex->extra.cubic.controller_2, vertex->base.coord, t, value);
	lua_pushnumber(l, value.x);
	lua_pushnumber(l, value.y);
}

static void			draw_shape_arc(t_vec *last_point, t_vertex *vertex,
					double t) {
	lua_pushnumber(l, vertex->base.coord.x);
	lua_pushnumber(l, vertex->base.coord.y);
	last_point = NULL;
	vertex = NULL;
	t = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

static void			(*vertex_functions[4])(t_vec*, t_vertex*, double) = {
	draw_shape_line,
	draw_shape_quad,
	draw_shape_cubic,
	draw_shape_arc
};

static size_t		vertex_sizes[4] = {
	sizeof(t_vertex_base),
	sizeof(t_vertex_base) + sizeof(t_vertex_quad),
	sizeof(t_vertex_base) + sizeof(t_vertex_cubic),
	sizeof(t_vertex_base) + sizeof(t_vertex_arc)
};

int					api_shape_get(lua_State *l) {
	t_shape_user	*shape;
	t_vertex		*vertex;
	t_vec			*last_point;
	int8_t			*buffer;
	double			position, wanted_position;
	double			t;
	size_t			i;

	/// GET PARAMS
	shape = lua_touserdata(l, 1);
	wanted_position = get_param_float(2);
	if (wanted_position > 1)
		wanted_position -= (int64_t)wanted_position;
	/// LOOP THROUGH VERTICES
	position = 0;
	last_point = NULL;
	buffer = (int8_t*)&(shape->vertices);
	for (i = 0; i < shape->num_vertices; ++i) {
		/// GET VERTEX
		vertex = (t_vertex*)buffer;
		/// CHECK SEGMENT
		if (position + vertex->base.length > wanted_position) {
			/// SEND VALUE
			t = (wanted_position - position) / vertex->base.length;
			vertex_functions[vertex->base.type](last_point, vertex, t);
			break;
		}
		position += vertex->base.length;
		last_point = &(vertex->base.coord);
		/// OFFSET BUFFER
		buffer += vertex_sizes[vertex->base.type];
	}
	return 2;
}
