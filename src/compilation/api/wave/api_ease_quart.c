/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			api_ease_quart_in(lua_State *l) {
	lua_pushnumber(l, api_wave_remap(l, ease_quart_in(get_param_float(1)), 1));
	return 1;
}

int		api_ease_quart_out(lua_State *l) {
	lua_pushnumber(l, api_wave_remap(l, ease_quart_out(get_param_float(1)), 1));
	return 1;
}

int		api_ease_quart(lua_State *l) {
	lua_pushnumber(l, api_wave_remap(l, ease_quart(get_param_float(1)), 1));
	return 1;
}
