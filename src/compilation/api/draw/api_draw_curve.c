/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			api_draw_curve(lua_State *l) {
	t_vec	p0, p1, p2, p3;
	lint	points;

	/// GET PARAMS
	p0 = get_param_coord(1);
	p1 = get_param_coord(3);
	p2 = get_param_coord(5);
	/// BEZIER CUBIC
	if (lua_isnumber(l, 8) == true) {
		p3 = get_param_coord(7);
		points = get_param_int(9);
		/// TRANSFORM POINTS IN SPACE
		apply_matrix_point(&p0);
		apply_matrix_point(&p1);
		apply_matrix_point(&p2);
		apply_matrix_point(&p3);
		/// DRAW LINE
		draw_bezier_cubic(p0, p1, p2, p3, points);
	/// BEZIER QUADRATIC
	} else {
		points = get_param_int(7);
		/// TRANSFORM POINTS IN SPACE
		apply_matrix_point(&p0);
		apply_matrix_point(&p1);
		apply_matrix_point(&p2);
		/// DRAW LINE
		draw_bezier_quad(p0, p1, p2, points);
	}
	l = NULL;
	return 0;
}
