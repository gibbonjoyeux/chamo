/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int			api_rand(lua_State *l) {
	double	from, to;
	double	range;

	/// MODE: MIN - MAX
	if (lua_isnumber(l, 2) == true) {
		from = get_param_float(1);
		to = get_param_float(2);
		range = (from < to) ? to - from : - (from - to);
		lua_pushnumber(l, from + ((double)rand() / RAND_MAX) * range);
	/// MODE: 0 - MAX
	} else if (lua_isnumber(l, 1) == true) {
		lua_pushnumber(l, ((double)rand() / RAND_MAX) * get_param_float(1));
	/// MODE: 0 - 1
	} else {
		lua_pushnumber(l, (double)rand() / RAND_MAX);
	}
	return 1;
}
