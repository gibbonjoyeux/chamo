/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		draw_polygon(t_vec center, double r, lint vertices, lint points,
			double angle) {
	t_vec	point;
	t_vec	last_point;
	double	point_angle;
	lint	line_points;
	lint	i;

	/// FOR EACH VERTEX
	for (i = 0; i <= vertices; ++i) {
		/// GET VERTEX
		point_angle = i * ((double)PI2 / (double)vertices) + (double)PI2
		/**/ * angle;
		point.x = center.x + cos(point_angle) * r;
		point.y = center.y + sin(point_angle) * r;
		/// DRAW LINE
		if (i > 0) {
			line_points = points / (vertices - (i - 1));
			draw_line(last_point, point, line_points);
			points -= line_points;
		}
		VEC_COPY(last_point, point);
	}
}
