/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		init_global_tables(void) {
	/// SHAPE METATABLE
	luaL_newmetatable(l, "_chamo_meta_shape");
	lua_pushvalue(l, -1);
	lua_setfield(l, -2, "__index");
	luaL_setfuncs(l, (luaL_Reg[]){
		{ "begin", api_shape_begin },
		{ "record", api_shape_record },
		{ "complete", api_shape_complete },
		{ "vertex", api_shape_vertex },
		{ "draw", api_shape_draw },
		{ "get", api_shape_get },
		{ NULL, NULL }
	}, 0);

	/// EVENTS TABLES
	lua_newtable(l);
	lua_pushnumber(l, 0);
	lua_setfield(l, -2, "x");
	lua_pushnumber(l, 0);
	lua_setfield(l, -2, "y");
	lua_pushboolean(l, false);
	lua_setfield(l, -2, "down");
	lua_setglobal(l, "mouse");
}
