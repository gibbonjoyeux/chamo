
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int		main(int argc, char **argv){
	/// NON REAL TIME MODE
	if (argc == 1
	|| (argc >= 2 && str_equ(argv[1], "-nrt") == true)) {
		puts("TERMINAL MODE");
		save_module(argc - 1, argv + 1);
	/// INTERACTIVE MODE
	} else {
		puts("INTERACTIVE MODE");
		init_sdl();
		play_module(argv[1]);
	}
	return 0;
}
