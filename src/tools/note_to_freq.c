/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "chamo.h"

static double	notes[] = {
	27.500, 29.135,	// A0 & a0
	30.867, 32.703,	// B0 & b0
	16.351, 17.323,	// C0 & c0
	18.354, 19.445,	// D0 & d0
	20.601, 21.826,	// E0 & e0
	21.826, 23.124,	// F0 & f0
	24.499, 25.956	// G0 & g0
};

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

double			note_to_freq(char *note) {
	double		freq;
	int			octave;

	if (note == NULL)
		return 0;
	/// GET NOTE
	if (note[0] >= 'A' && note[0] <= 'G')
		freq = notes[(note[0] - 'A') * 2];
	else if (note[0] >= 'a' && note[0] <= 'g')
		freq = notes[(note[0] - 'a') * 2 + 1];
	else
		return 0;
	/// GET OCTAVE
	octave = atoi(note + 1);
	/// COMPUTE FINAL NOTE
	while (octave > 0) {
		freq *= 2;
		octave -= 1;
	}
	return freq;
}
