
#include "chamo.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static void				call_user_init() {
	lua_getglobal(l, "init");
	lua_call(l, 0, 0);
}

static void				init_module_live() {
	c.live.last_point_x = 400;
	c.live.last_point_y = 400;
	c.live.chunk = NULL;
	c.live.play_index = 0;
}

static bool				init_osc_server(void) {
	/// INIT SERVER ADDR
	mem_clean(&(c.osc), sizeof(c.osc));
	c.osc.server.sin_family = AF_INET;
	c.osc.server.sin_port = htons(57130);
	c.osc.server.sin_addr.s_addr = INADDR_ANY;
	/// INIT SERVER SOCKET
	c.osc.socket = socket(PF_INET, SOCK_DGRAM, 0);
	/// BIND CONNECTION
	if (bind(c.osc.socket, (t_sockaddr*)&(c.osc.server),
	/**/ sizeof(c.osc.server)) < 0) {
		close(c.osc.socket);
		return false;
	}
	fcntl(c.osc.socket, F_SETFL, O_NONBLOCK);
	return true;
}

static void				close_osc_server(void) {
	close(c.osc.socket);
}

static void				osc_call(
	char				*buffer,
	int					param_count,
	int					len) {
	int					index_types;
	int					index_values;
	i32					value_i32;
	float				value_f32;
	int					i;

	/// SKIP PATH
	i = 0;
	while (buffer[i + 3] != 0)
		i += 4;
	/// SKIP TYPES
	i += 4;
	index_types = i;
	if (i < len) {
		/// SKIP TYPES
		while (buffer[i + 3] != 0)
			i += 4;
		/// PUSH PARAMETERS
		i += 4;
		index_values = i;
		while (buffer[index_types] != 0) {
			switch(buffer[index_types]) {
				/// PARAM INT
				case 'i':
					value_i32 = ((i32*)(buffer + index_values))[0];
					ENDIAN_BIG(value_i32);
					lua_pushnumber(l, value_i32);
					index_values += 4;
					break;
				/// PARAM FLOAT
				case 'f':
					value_f32 = ((float*)(buffer + index_values))[0];
					ENDIAN_BIG(value_f32);
					lua_pushnumber(l, value_f32);
					index_values += 4;
					break;
				/// PARAM STRING
				case 's':
					lua_pushstring(l, buffer + index_values);
					while (buffer[index_values + 3] != 0)
						index_values += 4;
					index_values += 4;
					break;
				/// PARAM UNHANDLED
				default:
					param_count -= 1;
					break;
			}
			param_count += 1;
			index_types += 1;
		}
		/// CALL FUNCTION
		lua_pcall(l, param_count, 0, 0);
	} else {
		lua_pop(l, 1);
	}
}

static bool				osc_get_path(
	char				*buffer,
	int					len) {
	char				key[64];
	int					i, j;

	i = 1;
	while (true) {
		/// GET PATH
		j = 0;
		while (buffer[i] != '/' && buffer[i] != 0) {
			key[j] = buffer[i];
			i += 1;
			j += 1;
		}
		key[j] = 0;
		/// PATH
		lua_getfield(l, -1, key);
		lua_remove(l, -2);
		//// PATH DIRECTORY
		if (buffer[i] == '/') {
			if (lua_istable(l, -1) == false) {
				lua_pop(l, 1);
				return false;
			}
			i += 1;
		//// PATH FUNCTION
		} else if (buffer[i] == 0) {
			if (lua_isfunction(l, -1) == false) {
				lua_pop(l, 1);
				return false;
			}
			return true;
		}
	}
	(void)len;
}

static void				handle_events(void) {
	char				buffer[4096];
	char				key[2] = "0";
	bool				state;
	int					len;
	SDL_Event			e;

	/// OSC EVENTS
	len = recvfrom(c.osc.socket, &(buffer[0]), 1024, 0, NULL, NULL);
	if (len > 0) {
		pthread_mutex_lock(&(c.mutex));
			lua_getglobal(l, "osc");
			/// MODE FUNCTION
			if (lua_isfunction(l, -1) == true) {
				lua_pushstring(l, &(buffer[0]));
				osc_call(&(buffer[0]), 1, len);
			/// MODE PATH
			} else if (lua_istable(l, -1) == true) {
				if (osc_get_path(buffer, len) == true)
					osc_call(&(buffer[0]), 0, len);
			/// MODE NONE
			} else {
				lua_pop(l, 1);
			}
		pthread_mutex_unlock(&(c.mutex));
	}
	/// SDL EVENTS
	while (SDL_PollEvent(&e) != 0) {
		if (e.type == SDL_QUIT
		|| (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)) {
			c.quit = true;
		} else if (e.type == SDL_MOUSEMOTION) {
			pthread_mutex_lock(&(c.mutex));
				lua_getglobal(l, "mouse");
				lua_pushnumber(l, MAP(e.motion.x, 0, 800,
				/**/ w.format.map_from.x, w.format.map_to.x));
				lua_setfield(l, -2, "x");
				lua_pushnumber(l, MAP(e.motion.y, 0, 800,
				/**/ w.format.map_from.y, w.format.map_to.y));
				lua_setfield(l, -2, "y");
				lua_pop(l, 1);
			pthread_mutex_unlock(&(c.mutex));
		} else if (e.type == SDL_MOUSEBUTTONDOWN) {
			pthread_mutex_lock(&(c.mutex));
				lua_getglobal(l, "mouse");
				lua_pushboolean(l, true);
				lua_setfield(l, -2, "down");
				lua_pop(l, 1);
			pthread_mutex_unlock(&(c.mutex));
		} else if (e.type == SDL_MOUSEBUTTONUP) {
			pthread_mutex_lock(&(c.mutex));
				lua_getglobal(l, "mouse");
				lua_pushboolean(l, false);
				lua_setfield(l, -2, "down");
				lua_pop(l, 1);
			pthread_mutex_unlock(&(c.mutex));
		} else if (e.type == SDL_KEYDOWN) {
			pthread_mutex_lock(&(c.mutex));
				lua_getglobal(l, "keydown");
				state = false;
				if (lua_type(l, -1) == LUA_TFUNCTION) {
					/// ALPHA
					if (e.key.keysym.sym >= SDLK_a
					&& e.key.keysym.sym <= SDLK_z) {
						key[0] = e.key.keysym.sym - SDLK_a + 'A';
						lua_pushstring(l, key);
						state = true;
					/// NUMERICAL
					} else if (e.key.keysym.sym >= SDLK_0
					&& e.key.keysym.sym <= SDLK_9) {
						key[0] = e.key.keysym.sym - SDLK_0 + '0';
						lua_pushstring(l, key);
						state = true;
					}
				}
				if (state == true) {
					lua_call(l, 1, 0);
				} else {
					lua_pop(l, 1);
				}
			pthread_mutex_unlock(&(c.mutex));
		} else if (e.type == SDL_KEYUP) {
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void					play_module(char *file_path) {
	/// MODE
	c.mode = MODE_LIVE;
	/// INIT
	pthread_mutex_init(&(c.mutex), NULL);
	init_osc_server();
	init_lua(file_path);
	init_chamo_struct();
	init_wave_struct();
	init_sdl_audio();
	call_user_init();
	/// SET BPS
	//wave_set_specs(w.header.sampleRate, LIVE_DEFAULT_BPS);
	/// PREPARE AUDIO
	init_module_live();
	SDL_PauseAudioDevice(c.live.audio_id, SDL_FALSE);
	/// RUN
	c.quit = false;
	while (c.quit == false) {
		handle_events();
	}
	/// CLOSE SDL
	close_osc_server();
	close_sdl_audio();
	close_sdl();
	close_lua();
	close_wave_struct();
}
